
install(FILES
        fixpressureconstraints.hh
        fixtemperatureconstraints.hh
        fixvelocityconstraints.hh
        gradeddgfcreator.hh
        onecomponentfluid.hh
        twocomponentfluid.hh
        velocitywallfunctions.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/appl/staggeredgrid/common)
