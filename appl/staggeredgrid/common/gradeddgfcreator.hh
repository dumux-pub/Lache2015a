// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/** \file
  *  \ingroup StaggeredModel
  *
  * \brief This file creates a dgf grid named temp.dgf.
  *
  * The minimum cell height is automatically chosen by the domain size and
  * grading factor. The grid can be defined at either of the one or both
  * boundary boxes.
  */

#ifndef DUMUX_GRADEDGFCREATOR_HH
#define DUMUX_GRADEDGFCREATOR_HH

#include<iomanip> 
#include<iostream>
#include<fstream>
#include<vector>
#include<dune/common/fvector.hh>

namespace Dumux {

/**
  * \brief Creates a dgf grid named temp.dgf.
  *
  * The minimum cell height is automatically chosen by the domain size and
  * grading factor. The grid can be defined at either of the one or both
  * boundary boxes.
  */
template<int dim>
class GradedDGFCreator
{
public:
  //! \brief Constructor
  GradedDGFCreator(Dune::FieldVector<double, dim> bBoxMin,
                   Dune::FieldVector<double, dim> bBoxMax,
                   Dune::FieldVector<unsigned, dim> noOfElements,
                   Dune::FieldVector<double, dim> minimumCellHeight,
                   Dune::FieldVector<double, dim> grading,
                   Dune::FieldVector<unsigned, dim> refinementDirection /* 0 lower, 1 upper, 2 both*/)
  {
    scalingFactor = 1.0;

    for (unsigned curDim = 0; curDim < dim; ++curDim)
    {
      if (noOfElements[curDim]%2 != 0)
      {
        std::cout << "GridDGFCreator should only be used for even number of cells" << std::endl;
      }

      length[curDim] = bBoxMax[curDim] - bBoxMin[curDim];
      if (refinementDirection[curDim] == 2 && grading[curDim] > 1.0)
      {
        noOfElements[curDim] = noOfElements[curDim] / 2.0;
        scalingFactor = 2.0;
      }

      if (noOfElements[curDim] == 0)
      {
        // last cell may be mishaped
        noOfElements[curDim] = std::ceil((std::log10(((grading[curDim] - 1.0) * length[curDim] + minimumCellHeight[curDim])
                                                     / minimumCellHeight[curDim]) - 1.0)
                                         / std::log10(grading[curDim]));
        createNodeVectors(bBoxMin, bBoxMin, noOfElements, minimumCellHeight, grading, refinementDirection, curDim);
      }
      else if (minimumCellHeight[curDim] < 1e-10 && grading[curDim] >= 1.0)
      {
        minimumCellHeight[curDim] = (grading[curDim] - 1.0) * length[curDim]
                                    / (std::pow(grading[curDim], noOfElements[curDim]) - 1.0);
        createNodeVectors(bBoxMin, bBoxMin, noOfElements, minimumCellHeight, grading, refinementDirection, curDim);
      }
      else if (grading[curDim] == 0)
      {
        std::cout << "GradedDGFCreator has no implementation for calculating the grading." << std::endl;
        std::cout << "Please choose the noOfElements or the grading to zero" << std::endl;
        exit(1);
      }
      else
      {
        std::cout << "GradedDGFCreator needs a zero value for noOfElements or grading." << std::endl;
        std::cout << "The zero value will then be adjusted." << std::endl;
        exit(1);
      }
    }

    if (dim == 2)
    {
      dgfWriter2d(dx[0], dx[1]);
    }
    else
    {
      std::cout << "GradedDGFCreator is not implemented for 1D or 3D." << std::endl;
      exit(1);
    }
  }

/*!
 * \brief Creates a vector containing all the nodes of the grid.
 */
void createNodeVectors(Dune::FieldVector<double, dim> bBoxMin,
                       Dune::FieldVector<double, dim> bBoxMax,
                       Dune::FieldVector<unsigned, dim> noOfElements,
                       Dune::FieldVector<double, dim> minimumCellHeight,
                       Dune::FieldVector<double, dim> grading,
                       Dune::FieldVector<unsigned, dim> refinementDirection, /* 0 lower, 1 upper, 2 both*/ 
                       unsigned curDim)
{
  if (grading[curDim] > 1.0)
  {
    dx[curDim].push_back(0.0);
    for (unsigned i = 0; i < noOfElements[curDim]-1; ++i)
    {
      double hI = minimumCellHeight[curDim] * std::pow<double>(grading[curDim], i);
      dx[curDim].push_back(dx[curDim][i] + hI / length[curDim]);
    }
    dx[curDim].push_back(1.0);
    dx_min[curDim] = dx[curDim][1] - dx[curDim][0];

    if (refinementDirection[curDim] == 1)
    {
      std::vector<double> temp = dx[curDim];
      for (unsigned i = 0; i < noOfElements[curDim] + 1; ++i)
      {
        dx[curDim][i] = 1.0 - temp[noOfElements[curDim] - i];
      }
    }

    double scalingFactor = 1.0;
    if (refinementDirection[curDim] == 2)
    {
      for (unsigned i = 1; i < noOfElements[curDim] + 1; ++i)
      {
        dx[curDim].push_back(2.0 - dx[curDim][noOfElements[curDim] - i]);
      }
      scalingFactor = 2.0;
    }

    for (unsigned i = 0; i < noOfElements[curDim] * scalingFactor + 1; ++i)
    {
      dx[curDim][i] = bBoxMin[curDim] + dx[curDim][i] * length[curDim] / scalingFactor;
    }
  }
  else // grading == 1.0
  {
    for (unsigned i = 0; i < noOfElements[curDim] + 1; ++i)
    {
      dx[curDim].push_back(bBoxMin[curDim] + length[curDim] * i / noOfElements[curDim]);
    }
    dx_min[curDim] = dx[curDim][1] - dx[curDim][0];
  }
  std::cout << "dx_min[" << curDim << "] = " << dx_min[curDim] << std::endl;
}

/*!
 * \brief Creates a 2D dgf grid named temp.dgf.
 */
void dgfWriter2d(std::vector<double> dx, std::vector<double> dy)
{
  std::stringstream stream("");
  stream << "temp.dgf";
  stream.clear();
  std::string string(stream.str());
  const char* fileName = string.c_str();
  std::fstream file;
  file.open(fileName, std::ios::out);

  file << "DGF" << std::endl;
  file << "Vertex" << std::endl;

  int z = 0;
  std::vector<int> nodeNo;
  nodeNo.clear();
  nodeNo.push_back(z);
  for (unsigned j = 0; j < dy.size(); ++j)
  {
    for (unsigned i = 0; i < dx.size(); ++i)
    {
      z++;
      nodeNo.push_back(z);
      file << std::setprecision(12) << std::setw(-15) << std::setfill('0') << dx[i] << " " << dy[j] << '\n';
//       std::fprintf(fileName, "%12.15f %12.15f\n", dx[i],dy[j])
    }
  }
  file << "#" << std::endl;
  file << "CUBE" << std::endl;

  z = -1;
  for (unsigned j = 0; j < dy.size()-1; ++j)
  {
    for (unsigned i = 0; i < dx.size()-1; ++i)
    {
      z++;
      file << nodeNo[z] << " " << nodeNo[z+1] << " " << nodeNo[z+dx.size()] << " " << nodeNo[z+dx.size()+1] << '\n';
    }
    z++;
  }
  file << "#" << std::endl;
  file << "BOUNDARYDOMAIN" << std::endl;
  file << "default 1" << std::endl;
  file << "#" << std::endl;
  file.close();
}

public:
  std::vector<double> dx[dim];
  double dx_min[dim];
  double length[dim];
  double scalingFactor;
};

} // namespace Dumux

#endif // DUMUX_GRADEDGFCREATOR_HH