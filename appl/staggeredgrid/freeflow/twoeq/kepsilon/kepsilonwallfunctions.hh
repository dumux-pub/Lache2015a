/** \file
  *  \ingroup StaggeredModel
  *
  * \brief This file contains different wall function approaches for
  *        \f$ k-\varepsilon \f$.
  *
  * The wall functions have to be called from the Dirichlet function of
  * the problem files.
  * The term "stored" in front of the variable name indicates, that this
  * variable is updated in the updateStoredValues function.
  */

#ifndef DUMUX_KEPSILON_WALLFUNCTIONS_HH
#define DUMUX_KEPSILON_WALLFUNCTIONS_HH

#include<dune/common/exceptions.hh>

#include"../kepsilon/kepsilonproperties.hh"
#include"../kepsilon/kepsilonproperties.hh"

namespace Dumux {
/**
  * \brief Returns wall functions values for \f$ k-\varepsilon \f$ equation.
  *
  * This class contains different wall function approaches and has to be called
  * from the Dirichlet function of the problem files.
  * The term "stored" in front of the variable name indicates, that this
  * variable is updated in the updateStoredValues function.
  *
  * \tparam TypeTag TypeTag of the problem
  */
template<class TypeTag>
class KEpsilonWallFunctions
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
  typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
  typedef typename GET_PROP_TYPE(TypeTag, MapperElement) MapperElement;
  typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
  enum { dim = GridView::dimension };

  typedef typename GridView::template Codim<0>::Entity Element;

  //! \brief Constructor
  KEpsilonWallFunctions(const GridView& gv, Problem& problem)
  : mapperElement(gv), problemPtr_(0)
  {
    storedDistanceToWall_.resize(mapperElement.size());
    storedVelocityGradient_.resize(mapperElement.size());
    storedKinematicViscosity_.resize(mapperElement.size());
    storedDistanceInWallCoordinates_.resize(mapperElement.size());
    for (unsigned int i = 0; i < mapperElement.size(); ++i)
    {
      storedDistanceToWall_[i] = 0.0;
      storedVelocityGradient_[i] = 12345;
      storedKinematicViscosity_[i] = 0.0;
      storedDistanceInWallCoordinates_[i] = 0.0;
    }
    wallFunctionModel_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, KEpsilon, WallFunctionModel);
    turbulentKineticEnergy_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, TurbulentKineticEnergy);
    dissipation_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, Dissipation);
    problemPtr_ = &problem;
  }

  //! \brief Returns friction velocity
  double frictionVelocity(const Element& e) const
  {
    return std::sqrt(storedKinematicViscosity_[mapperElement.map(e)]
                     * std::abs(storedVelocityGradient_[mapperElement.map(e)]));
  }

  /*! \brief Returns friction velocity obtained from the log law
   *  \todo The analytical log law should only be needed in the logarithmic region
   */
  double frictionVelocityLogLaw(const Element& e) const
  {
    Scalar kinematicViscosity = storedKinematicViscosity_[mapperElement.map(e)];
    Scalar velocityGradient = storedVelocityGradient_[mapperElement.map(e)];
    Scalar storedDistanceToWall = std::abs(storedDistanceToWall_[mapperElement.map(e)]);
    Scalar uStar = kinematicViscosity * std::abs(velocityGradient);
    Scalar velocity = velocityGradient * storedDistanceToWall;
    Scalar logLawOffsetB = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, KEpsilon, LogLawOffsetB);
//     unsigned int iteration = 0;
    Scalar uStarOld = uStar+1;
    for (unsigned int i = 0; i < 100 && std::abs(uStarOld/uStar-1.0) > 1e-2; ++i)
    {
      uStarOld = uStar;
//     std::cout << " storedDistanceToWall: " << storedDistanceToWall << std::endl;
//     std::cout << " uStarOld: " << uStarOld << std::endl;
//     std::cout << " kinematicViscosity: " << kinematicViscosity << std::endl;
//     std::cout << " velocity: " << velocity << std::endl;
      uStar = std::abs(velocity)
              / (1.0 / GET_PROP_VALUE(TypeTag, KarmanConstant)
                 * std::log(storedDistanceToWall * uStarOld
                 / kinematicViscosity) + logLawOffsetB);
//       iteration = i;
    }
//     std::cout << " uStar,i: " << uStar << " ,  " << iteration << std::endl;
    return uStar;
  }

  //! \brief Call wall function for turbulentKineticEnergy
  double turbulentKineticEnergyWallFunction(const Element& e) const
  {
    if (wallFunctionModel_ == Indices::noWallFunction)
    {
        return constantValueTurbulentKineticEnergy(e);
    }
    else if (wallFunctionModel_ == Indices::wilcox)
    {
        return standardWilcoxTurbulentKineticEnergy(e);
    }
    else
    {
        DUNE_THROW(Dune::NotImplemented, "This turbulent kinetic energy wall function method is not implemented.");
        return 0;
    }
  }
  //! \brief Call wall function for dissipation
  double dissipationWallFunction(const Element& e) const
  {
    if (wallFunctionModel_ == Indices::noWallFunction)
    {
        return constantValueDissipation(e);
    }
    else if (wallFunctionModel_ == Indices::wilcox)
    {
        return standardWilcoxDissipation(e);
    }
    else
    {
        DUNE_THROW(Dune::NotImplemented, "This dissipation wall function method is not implemented.");
        return 0;
    }
  }

  // (0) Constant value wall functions
  //! \brief Constant value for turbulentKineticEnergy
  double constantValueTurbulentKineticEnergy(const Element& e) const
  {
    return turbulentKineticEnergy_;
  }
  //! \brief Constant value for dissipation
  double constantValueDissipation(const Element& e) const
  {
    return dissipation_;
  }

  // (1) Standard wall functions from Wilcox - Turbulence modeling in CFD, p. 181
  //! \brief Standard wall function for turbulentKineticEnergy
  double standardWilcoxTurbulentKineticEnergy(const Element& e) const
  {
    return frictionVelocity(e) * frictionVelocity(e)
           / std::sqrt(problem_().cMu());
  }
  //! \brief Standard wall function for dissipation
  double standardWilcoxDissipation(const Element& e) const
  {
    return std::pow(problem_().cMu(), 0.75)
           * std::pow(standardWilcoxTurbulentKineticEnergy(e), 1.5)
           / GET_PROP_VALUE(TypeTag, KarmanConstant) / storedDistanceToWall_[mapperElement.map(e)];
  }

  MapperElement mapperElement;
  mutable std::vector<double> storedDistanceToWall_;
  mutable std::vector<double> storedVelocityGradient_;
  mutable std::vector<double> storedKinematicViscosity_;
  mutable std::vector<double> storedDistanceInWallCoordinates_;
  unsigned int wallFunctionModel_;
  Scalar turbulentKineticEnergy_;
  Scalar dissipation_;

protected:
  Problem &problem_()
  { return *problemPtr_; }
  const Problem &problem_() const
  { return *problemPtr_; }

  Problem *problemPtr_;
};

} // end namespace Dumux

#endif // DUMUX_KEPSILON_WALLFUNCTIONS_HH
