/**
 * \file
 * \ingroup StaggeredGrid
 * \ingroup BaseKEpsilonStaggeredGrid
 *
 * \brief Local operator for staggered grid discretization for steady-state
 * Navier-Stokes equation, modeled with \f$ k-\varepsilon \f$ - turbulence
 * models
 *
 * \copydoc EddyViscosityStaggeredGrid
 *
 * The eddy viscosity depends on values for \f$ k \f$ and \f$ \varepsilon \f$:
 * \f[
 *    \nu_{\alpha,\textrm{t}}
 *    = \nu_{\alpha,\textrm{t}} \left( k, \varepsilon \right)
 *    > 0
 * \f]
 *
 * Turbulent Kinetic Energy balance:
 * \f[
 *    \frac{\partial \left( k \right)}{\partial t}
 *    + \nabla \cdot \left( v_\alpha k \right)
 *    - \nabla \cdot \left( \left( \nu + \frac{\nu_\textrm{t}}{\sigma_\textrm{k}} \right) \nabla k \right)
 *    - 2 \nu_\textrm{t} S_{ij} \cdot S_{ij}
 *    + \varepsilon
 *    - q_{k}
 *    = 0
 * \f]
 * with \f$ S_{ij} = \frac{1}{2} \left[ \frac{\partial}{\partial x_i} v_j + \frac{\partial}{\partial x_j} v_i \right] \f$
 * and \f$ a_{ij} \cdot b_{ij} = \sum_{i,j} a_{ij} b_{ij} \f$.
 *
 * Dissipation balance:
 * \f[
 *    \frac{\partial \left( \varepsilon \right)}{\partial t}
 *    + \nabla \cdot \left( v_\alpha \varepsilon \right)
 *    - \nabla \cdot \left( \left( \nu + \frac{\nu_\textrm{t}}{\sigma_\varepsilon} \right) \nabla \varepsilon \right)
 *    - C_{1\varepsilon} \frac{\varepsilon}{k} 2 \nu_\textrm{t} S_{ij} \cdot S_{ij}
 *    + C_{2\varepsilon} \frac{\varepsilon^2}{k}
 *    - q_{\varepsilon}
 *    = 0
 * \f]
 *
 * The use of the kinematic fluid viscosity for the diffusion term in the \f$ k \f$
 * and the \f$ \varepsilon \f$ equations can be disabled by setting the property
 * <tt> KEpsilonDisableKinematicViscosity</tt>.
 * \note The kinematic fluid viscosity is included in [Wilcox, 2006], [OpenFOAM]
 *       and CFD online. It is not included in [Versteeg and Malalasekra, 2009]
 *       and [Pope 2006].
 *
 * The kinematic eddy viscosity is calculated as follows:
 * \f[
 *    \nu_\textrm{t} = C_\mu \frac{k^2}{\varepsilon}
 * \f]
 *
 * The constants have the values
 * \f[
 *   \sigma_k = 1.00 \;,\;
 *   \sigma_\varepsilon =1.30 \;,\;
 *   C_{1\varepsilon} = 1.44 \;,\;
 *   C_{2\varepsilon} = 1.92 \;,\;
 *   C_\mu = 0.09
 * \f]
 *
 * \note Be aware that the storage terms (the time dependent terms)
 * are not included here but are implemented in the transient part
 * of the local operator.
 */

#ifndef DUMUX_BASE_KEPSILON_STAGGERED_GRID_HH
#define DUMUX_BASE_KEPSILON_STAGGERED_GRID_HH

#include<cstddef>
#include<vector>

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/fmatrix.hh>

// #include<dune/istl/io.hh>

#include<dune/geometry/quadraturerules.hh>
#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/type.hh>

#include<dune/pdelab/gridfunctionspace/lfsindexcache.hh>
#include<dune/pdelab/gridfunctionspace/localfunctionspace.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/pattern.hh>

#include"kepsilonpropertydefaults.hh"

namespace Dune
{
  namespace PDELab
  {
    /**
     * \brief Local operator for staggered grid discretization solving
     * the steady-state kepsilon equations.
     *
     * \tparam TypeTag TypeTag of the problem
     */
    template<class TypeTag>
    class BaseKEpsilonStaggeredGrid
    {
    public:
      typedef typename GET_PROP_TYPE(TypeTag, LocalOperator) Implementation;
      typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;

      typedef typename GET_PROP_TYPE(TypeTag, BCType) BC;
      typedef typename GET_PROP_TYPE(TypeTag, SourceTurbulentKineticEnergyBalance) SourceTurbulentKineticEnergyBalance;
      typedef typename GET_PROP_TYPE(TypeTag, SourceDissipationBalance) SourceDissipationBalance;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletTurbulentKineticEnergy) DirichletTurbulentKineticEnergy;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletDissipation) DirichletDissipation;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannTurbulentKineticEnergy) NeumannTurbulentKineticEnergy;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannDissipation) NeumannDissipation;

      typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
      enum { dim = GridView::dimension };

      typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
      typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;
      typedef typename GET_PROP_TYPE(TypeTag, MapperElement) MapperElement;

      typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
      enum { turbulentKineticEnergyIdx = Indices::turbulentKineticEnergyIdx,
             dissipationIdx = Indices::dissipationIdx };

      enum { phaseIdx = Indices::phaseIdx };

      //! element stored data
      typedef std::vector<double> StoredScalar;
      mutable StoredScalar storedTurbulentKineticEnergy;
      mutable StoredScalar storedDissipation;
      mutable StoredScalar storedStressTensorScalarProduct;
      mutable StoredScalar storedProductionK;
      mutable StoredScalar storedProductionEpsilon;

      //! \brief Pattern assembly flags
      enum { doPatternVolume = true,
             doPatternSkeleton = true };

      //! \brief Residual assembly flags
      enum { doAlphaVolume = true,
             doAlphaSkeleton = true,
             doAlphaBoundary = true };

      //! \brief Constructor
      BaseKEpsilonStaggeredGrid(const BC& bc_,
                          const SourceTurbulentKineticEnergyBalance& sourceTurbulentKineticEnergyBalance_, const SourceDissipationBalance& sourceDissipationBalance_,
                          const DirichletTurbulentKineticEnergy& dirichletTurbulentKineticEnergy_, const DirichletDissipation& dirichletDissipation_,
                          const NeumannTurbulentKineticEnergy& neumannTurbulentKineticEnergy_, const NeumannDissipation& neumannDissipation_,
                          GridView gridView_, Problem& problem)
        : bc(bc_),
          sourceTurbulentKineticEnergyBalance(sourceTurbulentKineticEnergyBalance_), sourceDissipationBalance(sourceDissipationBalance_),
          dirichletTurbulentKineticEnergy(dirichletTurbulentKineticEnergy_), dirichletDissipation(dirichletDissipation_),
          neumannTurbulentKineticEnergy(neumannTurbulentKineticEnergy_), neumannDissipation(neumannDissipation_),
          gridView(gridView_), mapperElement(gridView), problemPtr_(0)
      {
        // properties
        enableAdvectionAveraging_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableAdvectionAveraging);
        enableDiffusionHarmonic_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableDiffusionHarmonic);
        kEpsilonEnableKinematicViscosity_= GET_PARAM_FROM_GROUP(TypeTag, bool, KEpsilon, EnableKinematicViscosity);
        kEpsilonModelConstants_ = GET_PARAM_FROM_GROUP(TypeTag, int, KEpsilon, ModelConstants);

        problemPtr_ = &problem;
        initialize(gridView);
      }


      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_volume_navierstokes
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume_kepsilon(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r,
                                 std::vector<DimVector> velocityFaces, Scalar pressure,
                                 Scalar massMoleFrac, Scalar temperature) const
      {
        // select the components from the subspaces
        typedef typename LFSU::template Child<turbulentKineticEnergyIdx>::Type LFSU_K;
        const LFSU_K& lfsu_k = lfsu.template child<turbulentKineticEnergyIdx>();
        typedef typename LFSU::template Child<dissipationIdx>::Type LFSU_E;
        const LFSU_E& lfsu_e = lfsu.template child<dissipationIdx>();

        Scalar elementVolume = eg.geometry().volume();

        // /////////////////////
        // evaluation of unknown, upwinding and averaging
        Scalar turbulentKineticEnergy = x(lfsu_k, 0);
        Scalar dissipation = x(lfsu_e, 0);
        Scalar kinematicEddyViscosity
                = calculateKinematicEddyViscosity(turbulentKineticEnergy, dissipation);
        asImp_().storedKinematicEddyViscosity[mapperElement.map(eg.entity())] = kinematicEddyViscosity;

        // scalar product of velocity gradient tensor is the stress tenosr S_ij
        Dune::FieldMatrix<Scalar, GridView::dimension, GridView::dimension> stressTensor(0.0);
        for (unsigned int i = 0; i < dim; ++i)
        {
          for (unsigned int j = 0; j < dim; ++j)
          {
            stressTensor[i][j] = 0.5 * asImp_().storedVelocityGradientTensor[mapperElement.map(eg.entity())][i][j];
            stressTensor[i][j] += 0.5 * asImp_().storedVelocityGradientTensor[mapperElement.map(eg.entity())][j][i];

            //! \todo should the dilation term be used here?
//            if (j == i)
//            {
//              stressTensor[i][j] -= 1.0 / 3.0 * asImp_().storedVelocityGradientTensor[mapperElement.map(eg.entity())][i][j];
//            }
          }
        }
        Scalar stressTensorScalarProduct = 0.0;
        for (unsigned int i = 0; i < dim; ++i)
        {
          for (unsigned int j = 0; j < dim; ++j)
          {
            stressTensorScalarProduct += stressTensor[i][j] * stressTensor[i][j];
          }
        }
        storedStressTensorScalarProduct[mapperElement.map(eg.entity())] = stressTensorScalarProduct;

        // /////////////////////
        // contribution to residual from element
        // calculate flux over staggered interface in each dimension
        /**
          * (1) \b Production term of <b> turbulent kinetic energy </b> balance equation<br>
          *
          * \f[
          *    - 2 \nu_\textrm{t} S_{ij} \cdot S_{ij}
          *    \Rightarrow - \int_V 2 \nu_\textrm{t} S_{ij} \cdot S_{ij}
          * \f]
          * \f[
          *    \alpha = - 2 \nu_\textrm{t} S_{ij} \cdot S_{ij} V_e
          * \f]
          */
        r.accumulate(lfsu_k, 0,
                     -2.0 * kinematicEddyViscosity * stressTensorScalarProduct
                     * elementVolume);

        storedProductionK[mapperElement.map(eg.entity())]
          = -2.0 * kinematicEddyViscosity * stressTensorScalarProduct * elementVolume;

        /**
          * (2) \b Production term of \b dissipation balance equation<br>
          *
          * \f[
          *    - C_{1\varepsilon} \frac{\varepsilon}{k} 2 \nu_\textrm{t} S_{ij} \cdot S_{ij}
          *    \Rightarrow - \int_V C_{1\varepsilon} \frac{\varepsilon}{k} 2 \nu_\textrm{t} S_{ij} \cdot S_{ij}
          * \f]
          * \f[
          *    \alpha = - C_{1\varepsilon} \frac{\varepsilon}{k} 2 \nu_\textrm{t} S_{ij} \cdot S_{ij} V_e
          * \f]
          */
        r.accumulate(lfsu_e, 0,
                     -2.0 * cOneEpsilon() * dissipation / turbulentKineticEnergy
                     * kinematicEddyViscosity * stressTensorScalarProduct
                     * elementVolume);

        storedProductionEpsilon[mapperElement.map(eg.entity())]
          = -2.0 * cOneEpsilon() * dissipation / turbulentKineticEnergy
            * kinematicEddyViscosity * stressTensorScalarProduct
            * elementVolume;

        /**
          * (3) \b Destruction term of <b> turbulent kinetic energy </b> balance equation<br>
          *
          * \f[
          *    \varepsilon
          *    \Rightarrow \int_V \varepsilon
          * \f]
          * \f[
          *    \alpha = \varepsilon V_e
          * \f]
          */
        r.accumulate(lfsu_k, 0,
                     1.0 * dissipation * elementVolume);

        /**
          * (4) \b Destruction term of \b dissipation balance equation<br>
          *
          * \f[
          *    C_{2\varepsilon} \frac{\varepsilon^2}{k}
          *    \Rightarrow \int_V C_{2\varepsilon} \frac{\varepsilon^2}{k}
          * \f]
          * \f[
          *    \alpha = C_{2\varepsilon} \frac{\varepsilon^2}{k} V_e
          * \f]
          */
        r.accumulate(lfsu_e, 0,
                     1.0 * cTwoEpsilon() * dissipation * dissipation
                     / turbulentKineticEnergy * elementVolume);

        /**
          * (5) \b Source term of <b> turbulent kinetic energy </b> balance equation<br>
          *
          * \f[
          *    - q_\textrm{k}
          *    \Rightarrow - \int_V q_\textrm{k}
          * \f]
          * \f[
          *    \alpha = - q_\textrm{k} V_e
          * \f]
          */
        Dune::GeometryType gt = eg.geometry().type();
        const int qorder = 4;
        const Dune::QuadratureRule<Scalar,dim>& rule = Dune::QuadratureRules<Scalar,dim>::rule(gt, qorder);
        // loop over quadrature points
        for (typename Dune::QuadratureRule<Scalar,dim>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
        {
          typename SourceTurbulentKineticEnergyBalance::Traits::RangeType sourceTurbulentKineticEnergyBalanceValue;
          sourceTurbulentKineticEnergyBalance.evaluate(eg.entity(), it->position(), sourceTurbulentKineticEnergyBalanceValue);
          r.accumulate(lfsu_k, 0,
                       -1.0 * sourceTurbulentKineticEnergyBalanceValue * elementVolume * it->weight());
        }

        /**
          * (6) \b Source term of \b dissipation balance equation<br>
          *
          * \f[
          *    - q_{\varepsilon}
          *    \Rightarrow - \int_V q_{\varepsilon}
          * \f]
          * \f[
          *    \alpha = - q_{\varepsilon} V_e
          * \f]
          */
        // loop over quadrature points
        for (typename Dune::QuadratureRule<Scalar,dim>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
        {
          typename SourceDissipationBalance::Traits::RangeType sourceDissipationBalanceValue;
          sourceDissipationBalance.evaluate(eg.entity(), it->position(), sourceDissipationBalanceValue);
          r.accumulate(lfsu_e, 0,
                       -1.0 * sourceDissipationBalanceValue * elementVolume * it->weight());
        }
      }


      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_skeleton_navierstokes
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_skeleton_kepsilon(const IG& ig,
                                   const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                                   const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                                   R& r_s, R& r_n,
                                   std::vector<DimVector> velocities_s, Scalar pressure_s,
                                   Scalar massMoleFrac_s, Scalar temperature_s,
                                   std::vector<DimVector> velocities_n, Scalar pressure_n,
                                   Scalar massMoleFrac_n, Scalar temperature_n) const
      {
        // select the components from the subspaces
        typedef typename LFSU::template Child<turbulentKineticEnergyIdx>::Type LFSU_K;
        const LFSU_K& lfsu_k_s = lfsu_s.template child<turbulentKineticEnergyIdx>();
        const LFSU_K& lfsu_k_n = lfsu_n.template child<turbulentKineticEnergyIdx>();
        typedef typename LFSU::template Child<dissipationIdx>::Type LFSU_E;
        const LFSU_E& lfsu_e_s = lfsu_s.template child<dissipationIdx>();
        const LFSU_E& lfsu_e_n = lfsu_n.template child<dissipationIdx>();

        // local position of cell and face centers
        const Dune::FieldVector<Scalar, dim>& insideCellCenterLocal =
          Dune::ReferenceElements<Scalar, dim>::general(ig.inside()->type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim>& outsideCellCenterLocal =
          Dune::ReferenceElements<Scalar, dim>::general(ig.outside()->type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim-1>& faceCenterLocal =
          Dune::ReferenceElements<Scalar, dim-1>::general(ig.geometry().type()).position(0, 0);

        // global position of cell and face centers
        Dune::FieldVector<Scalar, dim> insideCellCenterGlobal =
          ig.inside()->geometry().global(insideCellCenterLocal);
        Dune::FieldVector<Scalar, dim> outsideCellCenterGlobal =
          ig.outside()->geometry().global(outsideCellCenterLocal);
        Dune::FieldVector<Scalar, dim> faceCenterGlobal =
          ig.geometry().global(faceCenterLocal);

        // face normal
        const Dune::FieldVector<Scalar, dim>& faceUnitOuterNormal = ig.centerUnitOuterNormal();

        // evaluate orientation of intersection
        unsigned int normDim = 0;
        unsigned int tangDim = 1;
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          if (std::abs(faceUnitOuterNormal[curDim]) > 1e-10 && dim > 1)
          {
            normDim = curDim;
            tangDim = 1 - curDim;
          }
        }

        // face midpoints of all faces
        const unsigned int numFaces =
          Dune::ReferenceElements<Scalar, dim>::general(ig.inside()->type()).size(1);
        std::vector<Dune::FieldVector<Scalar, dim> > faceCentersLocal_s(numFaces);
        std::vector<Dune::FieldVector<Scalar, dim> > faceCentersGlobal_s(numFaces);
        for (unsigned int curFace = 0; curFace < numFaces; ++curFace)
        {
          faceCentersLocal_s[curFace] =
            Dune::ReferenceElements<Scalar, dim>::general(ig.inside()->geometry().type()).position(curFace, 1);
          faceCentersGlobal_s[curFace] = ig.inside()->geometry().global(faceCentersLocal_s[curFace]);
        }

        // face volume for integration
        Scalar faceVolume = ig.geometry().integrationElement(faceCenterLocal)
                            * Dune::ReferenceElements<Scalar, dim-1>::general(ig.geometry().type()).volume();

        // distances between face center and cell centers for rectangular shapes
        Scalar distanceInsideToFace = faceCenterGlobal[normDim] - insideCellCenterGlobal[normDim];
        Scalar distanceOutsideToFace = outsideCellCenterGlobal[normDim] - faceCenterGlobal[normDim];

        // /////////////////////
        // evaluation of unknown, upwinding and averaging

        // evaluate cell values and constants
        Scalar turbulentKineticEnergy_s = x_s(lfsu_k_s, 0);
        Scalar turbulentKineticEnergy_n = x_n(lfsu_k_n, 0);
        Scalar dissipation_s = x_s(lfsu_e_s, 0);
        Scalar dissipation_n = x_n(lfsu_e_n, 0);
        Scalar kinematicViscosity_s = asImp_().kinematicViscosity(pressure_s, temperature_s, massMoleFrac_s);
        Scalar kinematicViscosity_n = asImp_().kinematicViscosity(pressure_n, temperature_n, massMoleFrac_n);
        Scalar kinematicEddyViscosity_s
                = calculateKinematicEddyViscosity(turbulentKineticEnergy_s, dissipation_s);
        Scalar kinematicEddyViscosity_n
                = calculateKinematicEddyViscosity(turbulentKineticEnergy_n, dissipation_n);
        asImp_().storedKinematicEddyViscosity[mapperElement.map(*ig.inside())] = kinematicEddyViscosity_s;
        asImp_().storedKinematicEddyViscosity[mapperElement.map(*ig.outside())] = kinematicEddyViscosity_n;

        // averaging: distance weighted average for diffusion term
        Scalar kinematicViscosity_avg = (distanceInsideToFace * kinematicViscosity_n
                                      + distanceOutsideToFace * kinematicViscosity_s)
                                    / (distanceInsideToFace + distanceOutsideToFace);
        Scalar kinematicEddyViscosity_avg = (distanceInsideToFace * kinematicEddyViscosity_n
                                          + distanceOutsideToFace * kinematicEddyViscosity_s)
                                        / (distanceInsideToFace + distanceOutsideToFace);

        if (enableDiffusionHarmonic_)
        {
          // averaging: harmonic averages for diffusion term
          kinematicViscosity_avg = (2.0 * kinematicViscosity_n * kinematicViscosity_s)
                                  / (kinematicViscosity_n + kinematicViscosity_s);
          kinematicEddyViscosity_avg = (2.0 * kinematicEddyViscosity_n * kinematicEddyViscosity_s)
                                      / (kinematicEddyViscosity_n + kinematicEddyViscosity_s);
        }

        Scalar effectiveViscosityK_avg = kinematicEddyViscosity_avg / sigmaK();
        Scalar effectiveViscosityEpsilon_avg = kinematicEddyViscosity_avg / sigmaEpsilon();
        if (kEpsilonEnableKinematicViscosity_)
        {
          effectiveViscosityK_avg += kinematicViscosity_avg;
          effectiveViscosityEpsilon_avg += kinematicViscosity_avg;
        }

        // upwinding: advection term
        Scalar velocityNormal = (velocities_s[ig.indexInInside()] * faceUnitOuterNormal);
        Scalar turbulentKineticEnergy_up = turbulentKineticEnergy_s;
        Scalar dissipation_up = dissipation_s;
        if (velocityNormal < 0)
        {
          turbulentKineticEnergy_up = turbulentKineticEnergy_n;
          dissipation_up = dissipation_n;
        }

        if (enableAdvectionAveraging_)
        {
          turbulentKineticEnergy_up = (distanceInsideToFace * turbulentKineticEnergy_n
                                        + distanceOutsideToFace * turbulentKineticEnergy_s)
                                      / (distanceInsideToFace + distanceOutsideToFace);
          dissipation_up = (distanceInsideToFace * dissipation_n
                            + distanceOutsideToFace * dissipation_s)
                          / (distanceInsideToFace + distanceOutsideToFace);
        }

        // /////////////////////
        // contribution to residual on inside element, other residual is computed by symmetric call

        /**
         * (1) \b Flux term of <b> turbulent kinetic energy </b> balance equation
         *
         * \f[
         *    k v
         *    \Rightarrow \int_\gamma \left( k v \right) \cdot n
         * \f]
         * \f[
         *    \alpha_\textrm{self}
         *    = |\gamma| k_\textrm{up} \left( v \cdot n \right)
         * \f]
         *
         * The default value is \b upwinding for the advective part, by
         * using the macro <tt>ENABLE_ADVECTION_AVERAGING 1</tt> you can do an
         * averaging instead of upwinding for the turbulentKineticEnergy.
         */
        r_s.accumulate(lfsu_k_s, 0,
                       1.0 * turbulentKineticEnergy_up
                       * velocityNormal
                       * faceVolume);
        r_n.accumulate(lfsu_k_n, 0,
                       -1.0 * turbulentKineticEnergy_up
                       * velocityNormal
                       * faceVolume);

        /**
         * (2) \b Flux term of \b dissipation balance equation
         *
         * \f[
         *    \varepsilon v
         *    \Rightarrow \int_\gamma \left( \varepsilon v \right) \cdot n
         * \f]
         * \f[
         *    \alpha_\textrm{self}
         *    = |\gamma| \varepsilon_\textrm{up} \left( v \cdot n \right)
         * \f]
         *
         * The default value is \b upwinding for the advective part, by
         * using the macro <tt>ENABLE_ADVECTION_AVERAGING 1</tt> you can do an
         * averaging instead of upwinding for the dissipation.
         */
        r_s.accumulate(lfsu_e_s, 0,
                       1.0 * dissipation_up
                       * velocityNormal
                       * faceVolume);
        r_n.accumulate(lfsu_e_n, 0,
                       -1.0 * dissipation_up
                       * velocityNormal
                       * faceVolume);

        /**
         * (3) \b Diffusion term of <b> turbulent kinetic energy </b> balance equation
         *
         * \f[
         *    - \frac{\nu_\textrm{t}}{\sigma_\textrm{k}} \nabla k
         *    \Rightarrow - \int_\gamma \left( \left( \nu + \frac{\nu_\textrm{t}}{\sigma_\textrm{k}} \right) \nabla k \right) \cdot n
         * \f]
         * \f[
         *    \alpha_\textrm{self}
         *    = - |\gamma| \left( \nu + \frac{\nu_\textrm{t,avg}}{\sigma_\textrm{k,avg}} \right) \nabla k \cdot n
         * \f]
         *
         * The default procedure for averaging is a distance weighted average, by
         * using the macro <tt>ENABLE_DIFFUSION_HARMONIC 1</tt> you can do an
         * harmonic averaging instead.
         */
        r_s.accumulate(lfsu_k_s, 0,
                       -1.0 * effectiveViscosityK_avg
                       * (turbulentKineticEnergy_n - turbulentKineticEnergy_s)
                       / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                       * faceUnitOuterNormal[normDim]
                       * faceVolume);
        r_n.accumulate(lfsu_k_n, 0,
                       1.0 * effectiveViscosityK_avg
                       * (turbulentKineticEnergy_n - turbulentKineticEnergy_s)
                       / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                       * faceUnitOuterNormal[normDim]
                       * faceVolume);

        /**
         * (4) \b Diffusion term of \b dissipation balance equation
         *
         * \f[
         *    - \frac{\nu_\textrm{t}}{\sigma_\varepsilon} \nabla \varepsilon
         *    \Rightarrow - \int_\gamma \left( \left( \nu + \frac{\nu_\textrm{t}}{\sigma_\varepsilon} \right) \nabla \varepsilon \right) \cdot n
         * \f]
         * \f[
         *    \alpha_\textrm{self}
         *    = - |\gamma| \left( \nu + \frac{\nu_\textrm{t,avg}}{\sigma_{\varepsilon \textrm{,avg}}} \right) \nabla \varepsilon \cdot n
         * \f]
         *
         * The default procedure for averaging is a distance weighted average, by
         * using the macro <tt>ENABLE_DIFFUSION_HARMONIC 1</tt> you can do an
         * harmonic averaging instead.
         */
        r_s.accumulate(lfsu_e_s, 0,
                      -1.0 * effectiveViscosityEpsilon_avg
                       * (dissipation_n - dissipation_s)
                       / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                       * faceUnitOuterNormal[normDim]
                       * faceVolume);
        r_n.accumulate(lfsu_e_n, 0,
                      1.0 * effectiveViscosityEpsilon_avg
                       * (dissipation_n - dissipation_s)
                       / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                       * faceUnitOuterNormal[normDim]
                       * faceVolume);
      }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_boundary_navierstokes
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_boundary_kepsilon(const IG& ig, const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s, R& r_s,
                                   std::vector<DimVector> velocityFaces, Scalar pressure_s,
                                   Scalar massMoleFrac_s, Scalar temperature_s,
                                   Scalar pressure_boundary,
                                   Scalar massMoleFrac_boundary, Scalar temperature_boundary) const
      {
        // select the components from the subspaces
        typedef typename LFSU::template Child<turbulentKineticEnergyIdx>::Type LFSU_K;
        const LFSU_K& lfsu_k_s = lfsu_s.template child<turbulentKineticEnergyIdx>();
        typedef typename BC::template Child<turbulentKineticEnergyIdx>::Type BCTurbulentKineticEnergy;
        const BCTurbulentKineticEnergy& bcTurbulentKineticEnergy = bc.template child<turbulentKineticEnergyIdx>();
        typedef typename LFSU::template Child<dissipationIdx>::Type LFSU_E;
        const LFSU_E& lfsu_e_s = lfsu_s.template child<dissipationIdx>();
        typedef typename BC::template Child<dissipationIdx>::Type BCDissipation;
        const BCDissipation& bcDissipation = bc.template child<dissipationIdx>();

        // domain and range field type
        typedef typename LFSU_K::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;

        // center in face's reference element
        const Dune::FieldVector<Scalar, IG::dimension-1>& faceCenterLocal =
          Dune::ReferenceElements<Scalar, IG::dimension-1>::general(ig.geometry().type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim>& faceCenterGlobal =
          ig.geometry().global(faceCenterLocal);

        // face volume for integration
        RF faceVolume = ig.geometry().integrationElement(faceCenterLocal)
          * Dune::ReferenceElements<Scalar,IG::dimension-1>::general(ig.geometry().type()).volume();

        // calculate velocity DoF positions
        const unsigned int numControlVolumeFaces =
          Dune::ReferenceElements<Scalar, dim>::general(ig.inside()->geometry().type()).size(1);
        std::vector<Dune::FieldVector<Scalar, dim> > faceCentersLocal(numControlVolumeFaces);
        std::vector<Dune::FieldVector<Scalar, dim> > faceCentersGlobal(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          std::fill(faceCentersLocal[curFace].begin(), faceCentersLocal[curFace].end(), 0.5);
          faceCentersLocal[curFace][curFace / 2] = curFace % 2;
          faceCentersGlobal[curFace] =
            ig.inside()->geometry().global(faceCentersLocal[curFace]);
        }

        // cell center in reference element
        const Dune::FieldVector<Scalar,IG::dimension>&
          insideCellCenterLocal = Dune::ReferenceElements<Scalar,IG::dimension>::general(ig.inside()->type()).position(0, 0);
        Dune::FieldVector<Scalar, dim> insideCellCenterGlobal =
          ig.inside()->geometry().global(insideCellCenterLocal);

        // evaluate transformation which must be linear
        typename IG::Entity::Geometry::JacobianInverseTransposed jac;
        jac = ig.inside()->geometry().jacobianInverseTransposed(insideCellCenterLocal);
        jac.invert();

        const Dune::FieldVector<Scalar,dim>& faceUnitOuterNormal = ig.centerUnitOuterNormal();

        // evaluate orientation of intersection
        unsigned int normDim = 0;
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          if (std::abs(faceUnitOuterNormal[curDim]) > 1e-10 && dim > 1)
          {
            normDim = curDim;
          }
        }

        // evaluation of cell values
        Scalar turbulentKineticEnergy_s = x_s(lfsu_k_s, 0);
        Scalar dissipation_s = x_s(lfsu_e_s, 0);

        // /////////////////////
        // call ParentType
        //! \todo should this be an averaging here?
        Scalar kinematicViscosity_avg = asImp_().kinematicViscosity(pressure_s, temperature_s, massMoleFrac_s);
        Scalar kinematicEddyViscosity_avg = calculateKinematicEddyViscosity(turbulentKineticEnergy_s, dissipation_s);
          // turbulentKineticEnergy at the boundary is given
          typename DirichletTurbulentKineticEnergy::Traits::RangeType  turbulentKineticEnergy_boundary(0.0);
          dirichletTurbulentKineticEnergy.evaluate(*ig.inside(), faceCenterGlobal, turbulentKineticEnergy_boundary);
          // dissipation at the boundary is given
          typename DirichletDissipation::Traits::RangeType  dissipation_boundary(0.0);
          dirichletDissipation.evaluate(*ig.inside(), faceCenterGlobal, dissipation_boundary);
//         Scalar kinematicEddyViscosity_avg = calculateKinematicEddyViscosity(turbulentKineticEnergy_boundary, dissipation_boundary);
        asImp_().storedKinematicEddyViscosity[mapperElement.map(*ig.inside())] = kinematicEddyViscosity_avg;

        //! \todo should this be an averaging here?
        
        Scalar effectiveViscosityK_avg = kinematicEddyViscosity_avg / sigmaK();
        Scalar effectiveViscosityEpsilon_avg = kinematicEddyViscosity_avg / sigmaEpsilon();
        if (kEpsilonEnableKinematicViscosity_)
        {
          effectiveViscosityK_avg += kinematicViscosity_avg;
          effectiveViscosityEpsilon_avg += kinematicViscosity_avg;
        }

        // Inflow boundary for turbulentKineticEnergy
        if (bcTurbulentKineticEnergy.isInflow(ig, faceCenterLocal))
        {
          /**
           * Inflow boundary handling for turbulent kinetic energy balance<br>
           * (1) \b Flux term of <b> turbulent kinetic energy </b> balance equation
           * \f[
           *    k v
           *    \Rightarrow \int_\gamma \left( k v \right) \cdot n
           * \f]
           * \f[
           *    \alpha_\textrm{self}
           *    = |\gamma| k_\textrm{boundary} \left( v \cdot n \right)
           * \f]
           */
          r_s.accumulate(lfsu_k_s, 0,
                         1.0 * turbulentKineticEnergy_boundary
                         * (velocityFaces[ig.indexInInside()] * faceUnitOuterNormal)
                         * faceVolume);

        /**
         * (2) \b Diffusion term of <b> turbulent kinetic energy </b> balance equation
         *
         * \f[
         *    - \frac{\nu_\textrm{t}}{\sigma_\textrm{k}} \nabla k
         *    \Rightarrow - \int_\gamma \left( \left( \nu + \frac{\nu_\textrm{t}}{\sigma_\textrm{k}} \right) \nabla k \right) \cdot n
         * \f]
         * \f[
         *    \alpha_\textrm{self}
         *    = - |\gamma| \left( \nu + \frac{\nu_\textrm{t,self}}{\sigma_\textrm{k,self}} \right) \nabla k \cdot n
         * \f]
         *
         * At the boundary the values from the inner cell are taken and <b>no averaging</b>
         * is performed.
         */
         // The default procedure for averaging is a distance weighted average, by
         // using the macro <tt>ENABLE_DIFFUSION_HARMONIC 1</tt> you can do an
         // harmonic averaging instead.
        r_s.accumulate(lfsu_k_s, 0,
                      -1.0 * effectiveViscosityK_avg
                       * (turbulentKineticEnergy_boundary - turbulentKineticEnergy_s)
                       / (faceCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                       * faceUnitOuterNormal[normDim]
                       * faceVolume);
        }
        // Wall boundary for turbulentKineticEnergy
        else if (bcTurbulentKineticEnergy.isWall(ig, faceCenterLocal))
        {
          //! Nothing has to be done in case of Wall Condition for turbulentKineticEnergy.
          //! Dirichlet value will be set automatically.
        }
        // Outflow boundary for turbulentKineticEnergy
        else if (bcTurbulentKineticEnergy.isOutflow(ig, faceCenterLocal))
        {
          /**
           * Outflow boundary handling for turbulent kinetic energy balance<br>
           * (1) \b Flux term of <b> turbulent kinetic energy </b> balance equation
           * \f[
           *    k v
           *    \Rightarrow \int_\gamma \left( k v \right) \cdot n
           * \f]
           * \f[
           *    \alpha_\textrm{self}
           *    = |\gamma| k_\textrm{self} \left( v \cdot n \right)
           * \f]
           */
          r_s.accumulate(lfsu_k_s, 0,
                         1.0 * turbulentKineticEnergy_s
                         * (velocityFaces[ig.indexInInside()] * faceUnitOuterNormal)
                         * faceVolume);
        }
        // Symmetry boundary for turbulentKineticEnergy
        else if (bcTurbulentKineticEnergy.isSymmetry(ig, faceCenterLocal))
        {
          //! Nothing has to be done in case of Symmetry Condition for turbulentKineticEnergy.
          //! Ensure normal velocity = 0 is done by the parent function.
        }
        else
        {
          DUNE_THROW(Dune::NotImplemented, "Wrong boundary condition type for turbulentKineticEnergy.");
        }


        // Inflow boundary for dissipation
        if (bcDissipation.isInflow(ig, faceCenterLocal))
        {
          /**
           * Outflow boundary handling for dissipation balance<br>
           * (1) \b Flux term of \b dissipation balance equation
           * \f[
           *    \varepsilon v
           *    \Rightarrow \int_\gamma \left( \varepsilon v \right) \cdot n
           * \f]
           * \f[
           *    \alpha_\textrm{self}
           *    = |\gamma| \varepsilon_\textrm{boundary} \left( v \cdot n \right)
           * \f]
           */
          r_s.accumulate(lfsu_e_s, 0,
                         1.0 * dissipation_boundary
                         * (velocityFaces[ig.indexInInside()] * faceUnitOuterNormal)
                         * faceVolume);

        /**
         * (2) \b Diffusion term of \b dissipation balance equation
         *
         * \f[
         *    - \frac{\nu_\textrm{t}}{\sigma_\varepsilon} \nabla \varepsilon
         *    \Rightarrow - \int_\gamma \left( \frac{\nu_\textrm{t}}{\sigma_\varepsilon} \nabla \varepsilon \right) \cdot n
         * \f]
         * \f[
         *    \alpha_\textrm{self}
         *    = - |\gamma| \frac{\nu_\textrm{t,self}}{\sigma_{\varepsilon\textrm{,self}}} \nabla \varepsilon \cdot n
         * \f]
         *
         * At the boundary the values from the inner cell are taken and <b>no averaging</b>
         * is performed.
         */
         // The default procedure for averaging is a distance weighted average, by
         // using the macro <tt>ENABLE_DIFFUSION_HARMONIC 1</tt> you can do an
         // harmonic averaging instead.
        r_s.accumulate(lfsu_e_s, 0,
                      -1.0 * effectiveViscosityEpsilon_avg
                       * (dissipation_boundary - dissipation_s)
                       / (faceCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                       * faceUnitOuterNormal[normDim]
                       * faceVolume);
        }
        // Wall boundary for dissipation
        else if (bcDissipation.isWall(ig, faceCenterLocal))
        {
          //! Nothing has to be done in case of Wall Condition for dissipation.
          //! Dirichlet value will be set automatically.
        }
        // Outflow boundary for dissipation
        else if (bcDissipation.isOutflow(ig, faceCenterLocal))
        {
          /**
           * Outflow boundary handling for dissipation balance<br>
           * (1) \b Flux term of \b dissipation balance equation
           * \f[
           *    \varepsilon v
           *    \Rightarrow \int_\gamma \left( \varepsilon v \right) \cdot n
           * \f]
           * \f[
           *    \alpha_\textrm{self}
           *    = |\gamma| \varepsilon_\textrm{self} \left( v \cdot n \right)
           * \f]
           */
          r_s.accumulate(lfsu_e_s, 0,
                         1.0 * dissipation_s
                         * (velocityFaces[ig.indexInInside()] * faceUnitOuterNormal)
                         * faceVolume);
        }
        // Symmetry boundary for dissipation
        else if (bcDissipation.isSymmetry(ig, faceCenterLocal))
        {
          //! Nothing has to be done in case of Symmetry Condition for velocity.
          //! Ensure normal velocity = 0 is done by the parent function.
        }
        else
        {
          DUNE_THROW(Dune::NotImplemented, "Wrong boundary condition type for dissipation.");
        }
      }

      /**
       * \brief Initialize vector-stored values used for alpha_* routines.
       *
       * \tparam GridView GridView type
       */
      void initialize(const GridView& gridView)
      {
        if (!kEpsilonEnableKinematicViscosity_)
        {
          std::cout << "kEpsilonEnableKinematicViscosity_ = false" << std::endl;
        }

        storedTurbulentKineticEnergy.resize(mapperElement.size());
        storedDissipation.resize(mapperElement.size());
        storedStressTensorScalarProduct.resize(mapperElement.size());
        storedProductionK.resize(mapperElement.size());
        storedProductionEpsilon.resize(mapperElement.size());

        // initialize some of the values
        for (unsigned int i = 0; i < mapperElement.size(); ++i)
        {
          storedTurbulentKineticEnergy[i] = 0.0;
          storedDissipation[i] = 0.0;
          storedStressTensorScalarProduct[i] = 0.0;
          storedProductionK[i] = 0.0;
          storedProductionEpsilon[i] = 0.0;
        }

        // select the components from the subspaces
        typedef typename BC::template Child<turbulentKineticEnergyIdx>::Type BCTurbulentKineticEnergy;
        const BCTurbulentKineticEnergy& bcTurbulentKineticEnergy = bc.template child<turbulentKineticEnergyIdx>();
        typedef typename BC::template Child<dissipationIdx>::Type BCDissipation;
        const BCDissipation& bcDissipation = bc.template child<dissipationIdx>();


        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        typedef typename GridView::IntersectionIterator IntersectionIterator;
        typedef double Scalar;

        // loop over grid view to get elements with a wall intersection
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          for (IntersectionIterator ig = gridView.ibegin(*eit);
              ig != gridView.iend(*eit); ++ig)
          {
            // local and global position of face centers
            const Dune::FieldVector<Scalar, dim-1>& faceCenterLocal =
              Dune::ReferenceElements<Scalar, dim-1>::general(ig->geometry().type()).position(0, 0);
            Dune::FieldVector<Scalar, dim> faceCenterGlobal = ig->geometry().global(faceCenterLocal);

            //! \todo before there was a check for differently defined boundary conditions
            //!       for the different balance equations. maybe this has to be done somewhere else

            // check for multiple defined boundary conditions
            unsigned int numberOfBCTypesAtPos = 0;
            if (bcTurbulentKineticEnergy.isWall(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcTurbulentKineticEnergy.isInflow(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcTurbulentKineticEnergy.isOutflow(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcTurbulentKineticEnergy.isSymmetry(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (numberOfBCTypesAtPos > 1)
            {
              std::cout << "BCTurbulentKineticEnergy at faceCenterGlobal " << faceCenterGlobal << std::endl;
              std::cout << "Wall      " << bcTurbulentKineticEnergy.isWall(*ig, faceCenterLocal) << std::endl;
              std::cout << "Inflow    " << bcTurbulentKineticEnergy.isInflow(*ig, faceCenterLocal) << std::endl;
              std::cout << "Outflow   " << bcTurbulentKineticEnergy.isOutflow(*ig, faceCenterLocal) << std::endl;
              std::cout << "Symmetry  " << bcTurbulentKineticEnergy.isSymmetry(*ig, faceCenterLocal) << std::endl;
              DUNE_THROW(Dune::NotImplemented, "Multiple boundary conditions for turbulentKineticEnergy at one point.");
            }

            // check for multiple defined boundary conditions
            numberOfBCTypesAtPos = 0;
            if (bcDissipation.isWall(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcDissipation.isInflow(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcDissipation.isOutflow(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcDissipation.isSymmetry(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (numberOfBCTypesAtPos > 1)
            {
              std::cout << "BCDissipation at faceCenterGlobal " << faceCenterGlobal << std::endl;
              std::cout << "Wall      " << bcDissipation.isWall(*ig, faceCenterLocal) << std::endl;
              std::cout << "Inflow    " << bcDissipation.isInflow(*ig, faceCenterLocal) << std::endl;
              std::cout << "Outflow   " << bcDissipation.isOutflow(*ig, faceCenterLocal) << std::endl;
              std::cout << "Symmetry  " << bcDissipation.isSymmetry(*ig, faceCenterLocal) << std::endl;
              DUNE_THROW(Dune::NotImplemented, "Multiple boundary conditions for dissipation at one point.");
            }
          }
        }
      }

      /**
       * \brief Update vector-stored values used for alpha_* routines.
       *
       * This function just calls the ParentType.
       *
       * \tparam GFS GridFunctionSpace type
       * \tparam X Coefficient vector
       * \param gfs Grid function space
       * \param lastSolution Coefficient vector from last Newton step or time step
       */
      template<typename GFS, typename X>
      void updateStoredValues(const GFS& gfs, X& lastSolution)
      {
        std::cout << "test";
        // select the components from the subspaces
        typedef typename BC::template Child<turbulentKineticEnergyIdx>::Type BCTurbulentKineticEnergy;
        const BCTurbulentKineticEnergy& bcTurbulentKineticEnergy = bc.template child<turbulentKineticEnergyIdx>();
        typedef typename BC::template Child<dissipationIdx>::Type BCDissipation;
        const BCDissipation& bcDissipation = bc.template child<dissipationIdx>();

        // constants and types
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        typedef typename GridView::IntersectionIterator IntersectionIterator;
        typedef double Scalar;

        // loop over grid view to get elements with a wall intersection
        for (ElementIterator eit = gridView.template begin<0>();
             eit != gridView.template end<0>(); ++eit)
        {
          //! \todo remove all useWallFUnctions_[elem] and call the bc...IsWall(global) (be careful, maybe it->boundary() has to be used)
          //!       or use a check for u+
          for (IntersectionIterator ig = gridView.ibegin(*eit);
               ig != gridView.iend(*eit); ++ig)
          {
            const Dune::FieldVector<Scalar, dim-1>& faceCenterLocal =
              Dune::ReferenceElements<Scalar, dim-1>::general(ig->geometry().type()).position(0, 0);
//             const Dune::FieldVector<Scalar, dim>& faceUnitOuterNormal = ig->centerUnitOuterNormal();
//             unsigned int normDim = 1;
//             unsigned int tangDim = 0;
//             for (unsigned int curDim = 0; curDim < dim; ++curDim)
//             {
//               if (std::abs(faceUnitOuterNormal[curDim]) > 1e-10 && dim > 1)
//               {
//                 normDim = curDim;
//                 tangDim = 1 - curDim;
//               }
//             }

            unsigned int wallNormalAxis = asImp_().storedCorrespondingWallNormalAxis[mapperElement.map(*eit)];
            unsigned int flowNormalAxis = asImp_().storedCorrespondingFlowNormalAxis[mapperElement.map(*eit)];

            if (bcTurbulentKineticEnergy.isWall(*ig, faceCenterLocal))
            {
              dirichletTurbulentKineticEnergy.storedVelocityGradient_[mapperElement.map(*eit)]
                = asImp_().storedVelocityGradientTensor[mapperElement.map(*eit)][flowNormalAxis][wallNormalAxis];
              dirichletTurbulentKineticEnergy.storedDistanceToWall_[mapperElement.map(*eit)]
                = asImp_().storedDistanceToWall[mapperElement.map(*eit)];
              dirichletTurbulentKineticEnergy.storedKinematicViscosity_[mapperElement.map(*eit)]
                = asImp_().storedKinematicViscosity[mapperElement.map(*eit)];
              dirichletTurbulentKineticEnergy.storedDistanceInWallCoordinates_[mapperElement.map(*eit)]
                = asImp_().storedDistanceInWallCoordinates[mapperElement.map(*eit)];
            }
            if (bcDissipation.isWall(*ig, faceCenterLocal))
            {
              dirichletDissipation.storedVelocityGradient_[mapperElement.map(*eit)]
                = asImp_().storedVelocityGradientTensor[mapperElement.map(*eit)][flowNormalAxis][wallNormalAxis];
              dirichletDissipation.storedDistanceToWall_[mapperElement.map(*eit)]
                = asImp_().storedDistanceToWall[mapperElement.map(*eit)];
              dirichletDissipation.storedKinematicViscosity_[mapperElement.map(*eit)]
                = asImp_().storedKinematicViscosity[mapperElement.map(*eit)];
              dirichletDissipation.storedDistanceInWallCoordinates_[mapperElement.map(*eit)]
                = asImp_().storedDistanceInWallCoordinates[mapperElement.map(*eit)];
            }
          }
        }

        // constants and types
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;

        // make local function space
        typedef Dune::PDELab::LocalFunctionSpace<GFS> LFS;
        typedef typename LFS::template Child<turbulentKineticEnergyIdx>::Type LFS_K;
        typedef typename LFS::template Child<dissipationIdx>::Type LFS_E;
        typedef Dune::PDELab::LFSIndexCache<LFS> LFSCache;
        typedef typename X::template ConstLocalView<LFSCache> XView;

        typedef typename LFS_K::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits FETraits_K;
        typedef typename LFS_E::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits FETraits_E;
        typedef typename FETraits_K::RangeFieldType RF;
        typedef typename FETraits_K::RangeType RangeType_k;
        typedef typename FETraits_E::RangeType RangeType_e;

        // loop over grid view
        for (ElementIterator eit = gfs.gridView().template begin<0>();
            eit != gfs.gridView().template end<0>(); ++eit)
        {
          LFS lfs(gfs);
          LFSCache lfsCache(lfs);
          XView xView(lastSolution);
          std::vector<RF> xLocal(lfs.maxSize());
          std::vector<RangeType_k> basisLocal(lfs.maxSize());

          // bind local function space to inside element
          lfs.bind(*eit);
          LFS_K lfs_k = lfs.template child<turbulentKineticEnergyIdx>();
          lfsCache.update();
          xView.bind(lfsCache);
          xView.read(xLocal);
          xView.unbind();

          // local and global position of cell center
          Dune::FieldVector<Scalar, dim> cellCenterLocal(0.5);

          // evaluate solution bound to grid function space for cell center
          RangeType_k turbulentKineticEnergy(0.0);
          lfs_k.finiteElement().localBasis().evaluateFunction(cellCenterLocal, basisLocal);
          for (unsigned int i = 0; i < lfs_k.size(); ++i)
          {
            turbulentKineticEnergy.axpy(xLocal[lfs_k.localIndex(i)], basisLocal[i]);
          }

          storedTurbulentKineticEnergy[mapperElement.map(*eit)] = turbulentKineticEnergy;
        }

        // loop over grid view
        for (ElementIterator eit = gfs.gridView().template begin<0>();
            eit != gfs.gridView().template end<0>(); ++eit)
        {
          LFS lfs(gfs);
          LFSCache lfsCache(lfs);
          XView xView(lastSolution);
          std::vector<RF> xLocal(lfs.maxSize());
          std::vector<RangeType_e> basisLocal(lfs.maxSize());

          // bind local function space to inside element
          lfs.bind(*eit);
          LFS_E lfs_e = lfs.template child<dissipationIdx>();
          lfsCache.update();
          xView.bind(lfsCache);
          xView.read(xLocal);
          xView.unbind();

          // local and global position of cell center
          Dune::FieldVector<Scalar, dim> cellCenterLocal(0.5);

          RangeType_e dissipation(0.0);
          lfs_e.finiteElement().localBasis().evaluateFunction(cellCenterLocal, basisLocal);
          for (unsigned int i = 0; i < lfs_e.size(); ++i)
          {
            dissipation.axpy(xLocal[lfs_e.localIndex(i)], basisLocal[i]);
          }

          storedDissipation[mapperElement.map(*eit)] = dissipation;
          asImp_().storedKinematicEddyViscosity[mapperElement.map(*eit)]
            = calculateKinematicEddyViscosity(storedTurbulentKineticEnergy[mapperElement.map(*eit)], storedDissipation[mapperElement.map(*eit)]);
        }
      }

      /**
       * \brief Calculate eddy viscosity
       */
      double calculateKinematicEddyViscosity(const double turbulentKineticEnergy,
                                             const double dissipation) const
      {
        return cMu() * turbulentKineticEnergy * turbulentKineticEnergy / dissipation;
      }

      /**
       * \brief Returns the turbulentKineticEnergy for a given element
       *
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       */
      template<typename EG, typename LFSU, typename X>
      const Scalar turbulentKineticEnergy(const EG& eg, const LFSU& lfsu, const X& x) const
      {
        typedef typename LFSU::template Child<turbulentKineticEnergyIdx>::Type LFSU_K;
        const LFSU_K& lfsu_k = lfsu.template child<turbulentKineticEnergyIdx>();
        return x(lfsu_k, 0);
      }

      /**
       * \brief Returns the dissipation for a given element
       *
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       */
      template<typename EG, typename LFSU, typename X>
      const Scalar dissipation(const EG& eg, const LFSU& lfsu, const X& x) const
      {
        typedef typename LFSU::template Child<dissipationIdx>::Type LFSU_E;
        const LFSU_E& lfsu_e = lfsu.template child<dissipationIdx>();
        return x(lfsu_e, 0);
      }
    //! \brief Returns the \$f C_\mu \$f constant
    const Scalar cMu() const
    {
      if(kEpsilonModelConstants_ == Indices::launderSharma)
          return 0.09;
      else if (kEpsilonModelConstants_ == Indices::mohammadi)
          return 0.09;
      else
        DUNE_THROW(Dune::NotImplemented, "This set of kepsilon model constants is not implemented.");
    }

    //! \brief Returns the \$f C_\mu \$f constant
    const Scalar sigmaK() const
    {
      if(kEpsilonModelConstants_ == Indices::launderSharma)
          return 1.0;
      else if (kEpsilonModelConstants_ == Indices::mohammadi)
          return 1.0;
      else
        DUNE_THROW(Dune::NotImplemented, "This set of kepsilon model constants is not implemented.");
    }

    //! \brief Returns the \$f C_\mu \$f constant
    const Scalar sigmaEpsilon() const
    {
      if(kEpsilonModelConstants_ == Indices::launderSharma)
          return 1.3;
      else if (kEpsilonModelConstants_ == Indices::mohammadi)
          return 1.3;
      else
        DUNE_THROW(Dune::NotImplemented, "This set of kepsilon model constants is not implemented.");
    }

    //! \brief Returns the \$f C_{1\varepsilon}  \$f constant
    const Scalar cOneEpsilon() const
    {
      if(kEpsilonModelConstants_ == Indices::launderSharma)
          return 1.44;
      else if (kEpsilonModelConstants_ == Indices::mohammadi)
          return 1.44;
      else
        DUNE_THROW(Dune::NotImplemented, "This set of kepsilon model constants is not implemented.");
    }

    //! \brief Returns the \$f C_{2\varepsilon} \$f constant
    const Scalar cTwoEpsilon() const
    {
      if(kEpsilonModelConstants_ == Indices::launderSharma)
          return 1.92;
      else if (kEpsilonModelConstants_ == Indices::mohammadi)
          return 2.06;
      else
        DUNE_THROW(Dune::NotImplemented, "This set of kepsilon model constants is not implemented.");
    }

private:
      const BC& bc;
      const SourceTurbulentKineticEnergyBalance& sourceTurbulentKineticEnergyBalance;
      const SourceDissipationBalance& sourceDissipationBalance;
      const DirichletTurbulentKineticEnergy& dirichletTurbulentKineticEnergy;
      const DirichletDissipation& dirichletDissipation;
      const NeumannTurbulentKineticEnergy& neumannTurbulentKineticEnergy;
      const NeumannDissipation& neumannDissipation;
      GridView gridView;
      MapperElement mapperElement;

      // properties
      bool enableAdvectionAveraging_;
      bool enableDiffusionHarmonic_;
      bool kEpsilonEnableKinematicViscosity_;
      unsigned int kEpsilonModelConstants_;

protected:
      //! Current implementation.
      Implementation &asImp_()
      { return *static_cast<Implementation*>(this); }
      //! Current implementation.
      const Implementation &asImp_() const
      { return *static_cast<const Implementation*>(this); }

      Problem &problem_()
      { return *problemPtr_; }
      const Problem &problem_() const
      { return *problemPtr_; }

      Problem *problemPtr_;
    };
  } // namespace PDELab
} // namespace Dune

#endif // DUMUX_BASE_KEPSILON_STAGGERED_GRID_HH
