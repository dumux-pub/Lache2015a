/**
 * \file
 * \ingroup StaggeredGrid
 * \ingroup EddyViscosityStaggeredGrid
 *
 * \brief Local operator for staggered grid discretization for steady-state
 * Navier-Stokes equation, modelled with eddy viscosity models.
 *
 * Mass balance:
 * \f[
 *    \frac{\partial \varrho_\alpha}{\partial t}
 *    + \nabla \cdot \left( \varrho_\alpha v_\alpha \right)
 *    - q_{\varrho}
 *    = 0
 * \f]
 *
 * Momentum balance:
 * \f[
 *    \frac{\partial \left( \varrho_\alpha v_\alpha \right)}{\partial t}
 *    + \nabla \cdot \left( p_\alpha I
 *      - \varrho_\alpha \nu_\alpha
 *        \left( \nabla v_\alpha \right)
 *      - \varrho_\alpha \nu_{\alpha,\textrm{t}}
 *        \left( \nabla v_\alpha \right)
 *      \right)
 *    - \varrho_\alpha g
 *    - q_{\varrho v}
 *    = 0
 * \f]
 *
 * \todo The transposed part of the viscous term is not included in the
 *       equations yet.
 *
 * The eddy viscosity depends on the chosen model, this could be
 * <ul>
 * <li>0-Equation or algebraic models (like Prandtl, Baldwin-Lomax)</li>
 * <li>1-Equation models (which are not part of dumux yet)</li>
 * <li>2-Equation models (like \f$ k-\varepsilon \f$ or \f$ k-\omega \f$)</li>
 * </ul>
 *
 * \note Be aware that the storage terms (the time dependent terms)
 * are not included here but are implemented in the transient part
 * of the standard Navier-Stokes local operator.
 */

#ifndef DUMUX_EDDY_VISCOSITY_STAGGERED_GRID_HH
#define DUMUX_EDDY_VISCOSITY_STAGGERED_GRID_HH

#include<cstddef>
#include<vector>

#include<dune/common/exceptions.hh>
#include<dune/common/fmatrix.hh>
#include<dune/common/fvector.hh>

#include<dune/geometry/quadraturerules.hh>
#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/type.hh>

#include<dune/grid/common/mcmgmapper.hh>

#include<dune/pdelab/gridfunctionspace/lfsindexcache.hh>
#include<dune/pdelab/gridfunctionspace/localfunctionspace.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/pattern.hh>

#include<appl/staggeredgrid/freeflow/navierstokes/navierstokes/basenavierstokesstaggeredgrid.hh>

#include"eddyviscositypropertydefaults.hh"


namespace Dune
{
    /**
      * \brief Layout template for faces
      *
      * This layout template is for use in the MultipleCodimMultipleGeomTypeMapper.
      * It selects only etities with <tt>dim = dimgrid-1</tt>.
      *
      * \tparam dimgrid The dimension of the grid.
      */
    template<int dimgrid> struct MCMGIntersectionLayout
    {
      /**
      * \brief Test whether entities of the given geometry type should be
      * included in the map
      */
      bool contains(Dune::GeometryType gt)
      {
        return gt.dim() == (dimgrid - 1);
      }
    };

  namespace PDELab
  {
    /**
     * \brief Local operator for staggered grid discretization for steady-state
     *        Navier-Stokes equation, modeled with eddy viscosity models.
     *
     * \tparam TypeTag TypeTag of the problem
     */
    template<class TypeTag>
    class EddyViscosityStaggeredGrid
    : public BaseNavierStokesStaggeredGrid<TypeTag>
    {
    public:
      typedef BaseNavierStokesStaggeredGrid<TypeTag> ParentTypeMassMomentum;

      typedef typename GET_PROP_TYPE(TypeTag, LocalOperator) Implementation;
      typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;

      typedef typename GET_PROP_TYPE(TypeTag, BCType) BC;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletVelocity) DirichletVelocity;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletPressure) DirichletPressure;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannVelocity) NeumannVelocity;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannPressure) NeumannPressure;
      typedef typename GET_PROP_TYPE(TypeTag, SourceMomentumBalance) SourceMomentumBalance;
      typedef typename GET_PROP_TYPE(TypeTag, SourceMassBalance) SourceMassBalance;

      typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
      enum { dim = GridView::dimension };

      typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
      typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;
      typedef typename GET_PROP_TYPE(TypeTag, MapperElement) MapperElement;
      typedef Dune::MultipleCodimMultipleGeomTypeMapper<GridView, Dune::MCMGIntersectionLayout> MapperIntersection;

      //! type to store the velocities and coordinates
      typedef std::vector<typename GridView::IndexSet::IndexType> StoredGridViewID;
      typedef std::vector<unsigned int> StoredUnsignedInt;
      typedef std::vector<double> StoredScalar;
      typedef std::vector<Dune::FieldVector<double, GridView::dimension> > StoredVector;
      typedef std::vector<Dune::FieldMatrix<double, GridView::dimension, GridView::dimension> > StoredMatrix;
      typedef std::vector<Dune::FieldVector<Dune::FieldVector<double, GridView::dimension>, 2*GridView::dimension-2> >
        StoredNeighborFaceCentersGlobal;
      typedef std::vector<Dune::FieldVector<double, 2*GridView::dimension-2> >
        StoredNeighborVelocities;

      StoredVector storedIntersectionCentersGlobal;
      StoredNeighborFaceCentersGlobal storedNeighborFaceCentersGlobal;
      StoredNeighborVelocities storedNeighborVelocities;
      std::vector<std::vector<std::vector<unsigned int>>> storedNeighborID;
      StoredMatrix storedVelocityGradientTensor;

      StoredVector storedCorrespondingWallGlobal;
      StoredGridViewID storedCorrespondingWallElementID;
      StoredGridViewID storedOppositeWallElementID;
      StoredUnsignedInt storedCorrespondingWallNormalAxis;
      StoredUnsignedInt storedCorrespondingFlowNormalAxis;
      StoredScalar storedDistanceToWall;
      StoredScalar storedVelocityInWallCoordinates;
      StoredScalar storedDistanceInWallCoordinates;
      StoredScalar storedRoughness;
      StoredScalar storedAdditionalRoughnessLength;
      std::vector<Dune::FieldVector<double, GridView::dimension> > storedVelocityMaximum;
      std::vector<Dune::FieldVector<double, GridView::dimension> > storedVelocityMinimum;

      //! Eddy viscosity models
      mutable StoredScalar storedKinematicEddyViscosity;

      //! \brief Pattern assembly flags
      enum { doPatternVolume = true,
             doPatternSkeleton = true };

      //! \brief Residual assembly flags
      enum { doAlphaVolume = true,
             doAlphaSkeleton = true,
             doAlphaBoundary = true };

      //! \brief Index of unknowns
      typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
      enum { velocityIdx = Indices::velocityIdx,
             pressureIdx = Indices::pressureIdx };

      //! \brief Constructor
      EddyViscosityStaggeredGrid(const BC& bc_,
                                 const SourceMomentumBalance& sourceMomentumBalance_, const SourceMassBalance& sourceMassBalance_,
                                 const DirichletVelocity& dirichletVelocity_, const DirichletPressure& dirichletPressure_,
                                 const NeumannVelocity& neumannVelocity_, const NeumannPressure& neumannPressure_,
                                 GridView gridView_)
        : ParentTypeMassMomentum(bc_,
              sourceMomentumBalance_, sourceMassBalance_,
              dirichletVelocity_, dirichletPressure_,
              neumannVelocity_, neumannPressure_,
              gridView_),
          bc(bc_),
          sourceMomentumBalance(sourceMomentumBalance_), sourceMassBalance(sourceMassBalance_),
          dirichletVelocity(dirichletVelocity_), dirichletPressure(dirichletPressure_),
          neumannVelocity(neumannVelocity_), neumannPressure(neumannPressure_),
          gridView(gridView_),
          mapperElement(gridView_), mapperIntersection(gridView_)
      {
        enableNavierStokes_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableNavierStokes);
        enableAdvectionAveraging_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableAdvectionAveraging);
        enableDiffusionHarmonic_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableDiffusionHarmonic);
        wallNormalAxis_ = GET_PARAM_FROM_GROUP(TypeTag, int, Problem, WallNormalAxis);
        flowNormalAxis_ = GET_PARAM_FROM_GROUP(TypeTag, int, Problem, FlowNormalAxis);

        initialize(gridView);
      }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_volume
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume_massmomentum(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r,
                                      std::vector<DimVector> velocityFaces, Scalar pressure,
                                      Scalar massMoleFrac, Scalar temperature) const
      {
        ParentTypeMassMomentum::alpha_volume_massmomentum(eg, lfsu, x, lfsv, r,
                                                          velocityFaces, pressure, massMoleFrac, temperature);

        // select the two components from the subspaces
        typedef typename LFSU::template Child<velocityIdx>::Type LFSU_V;
        const LFSU_V& lfsu_v = lfsu.template child<velocityIdx>();

        // domain and range field type
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;

        // /////////////////////
        // geometry information

        // velocity DoF positions
        const unsigned int numControlVolumeFaces =
          Dune::ReferenceElements<DF, dim>::general(eg.geometry().type()).size(1);
        std::vector<Dune::FieldVector<DF, dim> > faceCentersLocal(numControlVolumeFaces);
        std::vector<Dune::FieldVector<DF, dim> > faceCentersGlobal(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          std::fill(faceCentersLocal[curFace].begin(), faceCentersLocal[curFace].end(), 0.5);
          faceCentersLocal[curFace][curFace/2] = curFace % 2;
          faceCentersGlobal[curFace] = eg.geometry().global(faceCentersLocal[curFace]);
        }

        // distance between two face mid points
        Dune::FieldVector<RF, dim> distancesFaceCenters(0.0);
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          distancesFaceCenters[curDim] =
            std::abs(faceCentersGlobal[2*curDim+1][curDim] - faceCentersGlobal[2*curDim][curDim]);
        }

        // staggered face volume (goes through cell center) perpendicular to each direction
        Dune::FieldVector<RF, dim> orthogonalFaceVolumes(1.0);
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          for (unsigned int normDim = 0; normDim < dim; ++normDim)
          {
            if (curDim != normDim)
            {
              orthogonalFaceVolumes[curDim] *= distancesFaceCenters[normDim];
            }
          }
        }

        // /////////////////////
        // evaluation of unknown, upwinding and averaging

        // evaluate fluid properties
        const Scalar density = asImp_().density(pressure, temperature, massMoleFrac);

        //! \note storedKinematicEddyViscosity[elementID] has to be set by the child function
        const typename GridView::IndexSet::IndexType elementID = mapperElement.map(eg.entity());
        DF kinematicEddyViscosity = asImp_().storedKinematicEddyViscosity[elementID];

        // /////////////////////
        // contribution to residual from element
        // calculate flux over staggered interface in each dimension

        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          /**
           * (1) \b Eddy term of \b momentum balance equation
           *
           * \f[
           *    - \boldsymbol{\tau}_\textrm{t}
           *    = - \nu_\textrm{t} \rho \nabla v
           *    \Rightarrow \int_\gamma - \boldsymbol{\tau} \cdot n
           *    = \int_\gamma - \nu_\textrm{t} \rho \nabla v \cdot n
           * \f]
           * normal case for all coordinate axes
           * \f[
           *    \alpha_\textrm{self}
           *    = - |\gamma| \left( \boldsymbol{\tau} \cdot n \right) \cdot n
           *    = - |\gamma| \varrho \nu_\textrm{t}
           *      \frac{\textrm{d} v_\textrm{curDim}}{\textrm{d} x_\textrm{curDim}}
           *    = - |\gamma| \varrho \nu_\textrm{t}
           *      \frac{v_{\textrm{right,curDim}} - v_{\textrm{left,curDim}}}
           *           {x_{\textrm{right,curDim}} - x_{\textrm{left,curDim}}}
           * \f]
           */
          if (enableNavierStokes_)
          {
            r.accumulate(lfsu_v, 2*curDim,
                        // normal is always positive
                        -1.0 * kinematicEddyViscosity * density
                        * (velocityFaces[curDim*2+1][curDim] - velocityFaces[curDim*2][curDim])
                        / (faceCentersGlobal[curDim*2+1][curDim] - faceCentersGlobal[curDim*2][curDim])
                        * orthogonalFaceVolumes[curDim]); // face volume
            r.accumulate(lfsu_v, 2*curDim+1,
                        // normal is always negative
                        1.0 * kinematicEddyViscosity * density
                        * (velocityFaces[curDim*2+1][curDim] - velocityFaces[curDim*2][curDim])
                        / (faceCentersGlobal[curDim*2+1][curDim] - faceCentersGlobal[curDim*2][curDim])
                        * orthogonalFaceVolumes[curDim]); // face volume
          }
        }
      }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_skeleton
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_skeleton_massmomentum(const IG& ig,
                                        const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                                        const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                                        R& r_s, R& r_n,
                                        std::vector<DimVector> velocities_s, Scalar pressure_s,
                                        Scalar massMoleFrac_s, Scalar temperature_s,
                                        std::vector<DimVector> velocities_n, Scalar pressure_n,
                                        Scalar massMoleFrac_n, Scalar temperature_n) const
      {
        ParentTypeMassMomentum::alpha_skeleton_massmomentum(ig, lfsu_s, x_s, lfsv_s, lfsu_n, x_n, lfsv_n, r_s, r_n,
                                                            velocities_s, pressure_s, massMoleFrac_s, temperature_s,
                                                            velocities_n, pressure_n, massMoleFrac_n, temperature_n);

        // select the two components from the subspaces
        typedef typename LFSU::template Child<velocityIdx>::Type LFSU_V;
        const LFSU_V& lfsu_v_s = lfsu_s.template child<velocityIdx>();
        const LFSU_V& lfsu_v_n = lfsu_n.template child<velocityIdx>();

        // domain and range field type
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangeVelocity;

        // /////////////////////
        // geometry information


        // local position of cell and face centers
        const Dune::FieldVector<DF, dim>& insideCellCenterLocal =
          Dune::ReferenceElements<DF, dim>::general(ig.inside()->type()).position(0, 0);
        const Dune::FieldVector<DF, dim>& outsideCellCenterLocal =
          Dune::ReferenceElements<DF, dim>::general(ig.outside()->type()).position(0, 0);
        const Dune::FieldVector<DF, dim-1>& faceCenterLocal =
          Dune::ReferenceElements<DF, dim-1>::general(ig.geometry().type()).position(0, 0);

        // global position of cell and face centers
        Dune::FieldVector<DF, dim> insideCellCenterGlobal =
          ig.inside()->geometry().global(insideCellCenterLocal);
        Dune::FieldVector<DF, dim> outsideCellCenterGlobal =
          ig.outside()->geometry().global(outsideCellCenterLocal);
        Dune::FieldVector<DF, dim> faceCenterGlobal =
          ig.geometry().global(faceCenterLocal);

        // face normal
        const Dune::FieldVector<DF, dim>& faceUnitOuterNormal = ig.centerUnitOuterNormal();

        // evaluate orientation of intersection
        unsigned int normDim = 0;
        // find out axis parallel to normal vector
        for (unsigned int curNormDim = 0; curNormDim < dim; ++curNormDim)
        {
          if (std::abs(faceUnitOuterNormal[curNormDim]) > 1e-10)
          {
            normDim = curNormDim;
          }
        }

        // face volume for integration
        RF faceVolume = ig.geometry().integrationElement(faceCenterLocal)
                        * Dune::ReferenceElements<DF, dim-1>::general(ig.geometry().type()).volume();

        // distances between face center and cell centers for rectangular shapes
        DF distanceInsideToFace = std::abs(faceCenterGlobal[normDim] - insideCellCenterGlobal[normDim]);
        DF distanceOutsideToFace = std::abs(outsideCellCenterGlobal[normDim] - faceCenterGlobal[normDim]);

        // /////////////////////
        // evaluation of unknown, upwinding and averaging

        // evaluate cell values
        const Scalar density_s = asImp_().density(pressure_s, temperature_s, massMoleFrac_s);
        const Scalar density_n = asImp_().density(pressure_n, temperature_n, massMoleFrac_n);

        //! \note storedKinematicEddyViscosity[elementID] has to be set by the child function
        const typename GridView::IndexSet::IndexType elementID_s = mapperElement.map(*ig.inside());
        const typename GridView::IndexSet::IndexType elementID_n = mapperElement.map(*ig.outside());
        Scalar kinematicEddyViscosity_s = asImp_().storedKinematicEddyViscosity[elementID_s];
        Scalar kinematicEddyViscosity_n = asImp_().storedKinematicEddyViscosity[elementID_n];

        // upwinding (from self to neighbor)
        Scalar velocityNormal = (velocities_s[ig.indexInInside()] * faceUnitOuterNormal);
        Scalar density_up = density_s;
        std::vector<RangeVelocity> velocities_up(velocities_s);
        if (velocityNormal < 0)
        {
          velocities_up = velocities_n;
          density_up = density_n;
        }

        // averaging: distance weighted average for diffusion term
        Scalar density_avg = (distanceInsideToFace * density_n + distanceOutsideToFace * density_s)
                             / (distanceInsideToFace + distanceOutsideToFace);
        Scalar kinematicEddyViscosity_avg = (distanceInsideToFace * kinematicEddyViscosity_n
                                              + distanceOutsideToFace * kinematicEddyViscosity_s)
                                            / (distanceInsideToFace + distanceOutsideToFace);

        if (enableDiffusionHarmonic_)
        {
          // averaging: harmonic averages for diffusion term
          density_avg = (2.0 * density_n * density_s) / (density_n + density_s);
          kinematicEddyViscosity_avg = (2.0 * kinematicEddyViscosity_n * kinematicEddyViscosity_s)
                                       / (kinematicEddyViscosity_n + kinematicEddyViscosity_s);
        }

        // /////////////////////
        // contribution to residual on inside element, other residual is computed by symmetric call

        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          /**
           * (1) \b Eddy term of \b momentum balance equation
           *
           * \f[
           *    - \boldsymbol{\tau}_\textrm{t}
           *    = - \nu_\textrm{t} \rho \nabla v
           *    \Rightarrow - \int_\gamma \boldsymbol{\tau} \cdot n
           *    = - \int_\gamma \nu_\textrm{t} \rho \left( \nabla v \cdot n \right)
           * \f]
           * Tangential cases for all coordinate axes (given for \b 2-D,
           * \b rectangular grids and \f$n=(0,1)^T\f$ which means balancing
           * momentum for \f$v_\textrm{0}\f$)<br>
           * \f[
           *    \alpha_\textrm{self}
           *    = - \left( \boldsymbol{\tau} \cdot n \right) \cdot t
           *    = - |\gamma| \varrho_\textrm{avg} \nu_\textrm{t,avg}
           *      \frac{\partial v_{0}}{\partial x_{1}}
           * \f]
           * Tangential case, for: \f$\frac{\partial v_{0}}{\partial x_{1}}\f$,
           * right and left means along \f$x_1\f$ axis, \f$n\f$ means 1st entry,
           * \f$t\f$ means 0th entry
           * \f[
           *    A
           *    = - |\gamma| \varrho_\textrm{avg} \nu_\textrm{t,avg}
           *      \frac{\partial v_{0}}{\partial x_{1}}
           *    = - |\gamma| \varrho_\textrm{avg} \nu_\textrm{t,avg}
           *      \left( 0.5 \left[\frac{\left(v_\textrm{right} - v_\textrm{left} \right|_\textrm{t}}
           *                  {\left| x_\textrm{right} - x_\textrm{left} \right|_\textrm{n}}
           *                \right]_\textrm{curDim}
           *           + 0.5 \left[ \frac{\left(v_\textrm{right} - v_\textrm{left} \right|_\textrm{t}}
           *                  {\left| x_\textrm{right} - x_\textrm{left} \right|_\textrm{n}}
           *                \right]_\textrm{curDim+1}
           *      \right)
           * \f]
           *
           * The default procedure for averaging
           * \f$\varrho_\textrm{avg}\f$, \f$\nu_\textrm{t,avg}\f$ is a distance
           * weighted average, by using the macro <tt>ENABLE_DIFFUSION_HARMONIC 1</tt>
           * one can do an harmonic averaging instead.
           */
          if(enableNavierStokes_)
          {
            // only tangential case, exclude normal case
            if (curDim != normDim)
            {
              unsigned int tangDim = curDim;
              r_s.accumulate(lfsu_v_s, 2*tangDim,
                            -0.5 * density_avg * kinematicEddyViscosity_avg
                            * (velocities_n[2*tangDim][tangDim] - velocities_s[2*tangDim][tangDim])
                              / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                            * faceUnitOuterNormal[normDim] * faceVolume);
              r_n.accumulate(lfsu_v_n, 2*tangDim,
                            0.5 * density_avg * kinematicEddyViscosity_avg
                            * (velocities_n[2*tangDim][tangDim] - velocities_s[2*tangDim][tangDim])
                              / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                            * faceUnitOuterNormal[normDim] * faceVolume);

              r_s.accumulate(lfsu_v_s, 2*tangDim+1,
                            -0.5 * density_avg * kinematicEddyViscosity_avg
                            * (velocities_n[2*tangDim+1][tangDim] - velocities_s[2*tangDim+1][tangDim])
                              / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                            * faceUnitOuterNormal[normDim] * faceVolume);
              r_n.accumulate(lfsu_v_n, 2*tangDim+1,
                            0.5 * density_avg * kinematicEddyViscosity_avg
                            * (velocities_n[2*tangDim+1][tangDim] - velocities_s[2*tangDim+1][tangDim])
                              / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                            * faceUnitOuterNormal[normDim] * faceVolume);
            }
          }
        }
      }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_boundary
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_boundary_massmomentum(const IG& ig, const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s, R& r_s,
                                        std::vector<DimVector> velocityFaces, Scalar pressure_s,
                                        Scalar massMoleFrac_s, Scalar temperature_s,
                                        Scalar pressure_boundary,
                                        Scalar massMoleFrac_boundary, Scalar temperature_boundary) const
      {
        ParentTypeMassMomentum::alpha_boundary_massmomentum(ig, lfsu_s, x_s, lfsv_s, r_s,
                                                            velocityFaces, pressure_s, massMoleFrac_s, temperature_s,
                                                            pressure_boundary, massMoleFrac_boundary, temperature_boundary);

        // select the two components from the subspaces
        typedef typename LFSU::template Child<velocityIdx>::Type LFSU_V;
        const LFSU_V& lfsu_v_s = lfsu_s.template child<velocityIdx>();
        typedef typename BC::template Child<velocityIdx>::Type BCVelocity;
        const BCVelocity& bcVelocity = bc.template child<velocityIdx>();

        // domain and range field type
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;

        // center in face's reference element
        const Dune::FieldVector<DF, dim-1>& faceCenterLocal =
          Dune::ReferenceElements<DF, dim-1>::general(ig.geometry().type()).position(0, 0);
        const Dune::FieldVector<DF, dim>& faceCenterGlobal =
          ig.geometry().global(faceCenterLocal);

        // face properties (coordinates and normal)
        const Dune::FieldVector<DF, dim>& insideFaceCenterLocal =
          ig.geometryInInside().global(faceCenterLocal);
        const Dune::FieldVector<DF,dim>& faceUnitOuterNormal = ig.centerUnitOuterNormal();

        // evaluate orientation of intersection
        unsigned int normDim = 0;
        // find out axis parallel to normal vector
        for (unsigned int curNormDim = 0; curNormDim < dim; ++curNormDim)
        {
          if (std::abs(faceUnitOuterNormal[curNormDim]) > 1e-10)
          {
            normDim = curNormDim;
          }
        }

        // face volume for integration
        RF faceVolume = ig.geometry().integrationElement(faceCenterLocal)
          * Dune::ReferenceElements<DF, dim-1>::general(ig.geometry().type()).volume();

        // cell center in reference element
        const Dune::FieldVector<DF,dim>&
          insideCellCenterLocal = Dune::ReferenceElements<DF, dim>::general(ig.inside()->type()).position(0, 0);
        const Dune::FieldVector<DF, dim> insideCellCenterGlobal =
          ig.inside()->geometry().global(insideCellCenterLocal);

        // /////////////////////
        // evaluation of unknown, upwinding and averaging
        const Scalar density_s = asImp_().density(pressure_s, temperature_s, massMoleFrac_s);

        //! \note storedKinematicEddyViscosity[elementID_s] has to be set by the child function
        const typename GridView::IndexSet::IndexType elementID_s = mapperElement.map(*ig.inside());
        DF kinematicEddyViscosity_s = asImp_().storedKinematicEddyViscosity[elementID_s];

        // Wall or Inflow boundary for velocity
        if (bcVelocity.isWall(ig, faceCenterLocal) || bcVelocity.isInflow(ig, faceCenterLocal))
        {
          for (unsigned int tangDim = 0; tangDim < dim; ++tangDim)
          {
            // only tangential case, exclude normal case
            if (tangDim != normDim)
            {
              // evaluate boundary condition functions
              typename DirichletVelocity::Traits::RangeType dirichletVelocityCenter;
              typename DirichletVelocity::Traits::RangeType dirichletVelocityCorner0;
              typename DirichletVelocity::Traits::RangeType dirichletVelocityCorner1;
              Dune::FieldVector<DF, dim> insideFaceCornerLocal0(insideFaceCenterLocal);
              Dune::FieldVector<DF, dim> insideFaceCornerLocal1(insideFaceCenterLocal);
              insideFaceCornerLocal0[tangDim] = 0.0;
              insideFaceCornerLocal1[tangDim] = 1.0;

              dirichletVelocity.evaluate(*(ig.inside()), insideFaceCenterLocal, dirichletVelocityCenter);
              dirichletVelocity.evaluate(*(ig.inside()), insideFaceCornerLocal0, dirichletVelocityCorner0);
              dirichletVelocity.evaluate(*(ig.inside()), insideFaceCornerLocal1, dirichletVelocityCorner1);

              /**
               * Dirichlet boundary handling for velocity/ momentum balance<br>
               * (1) \b Eddy term of \b momentum balance equation
               *
               * \f[
               *    - \boldsymbol{\tau}_\textrm{t}
               *    = - \nu_\textrm{t} \rho \left( \nabla v \right)
               *    \Rightarrow - \int_\gamma \boldsymbol{\tau} \cdot n
               *    = - \int_\gamma \nu_\textrm{t} \rho \nabla v \cdot n
               * \f]
               * Only the tangential case is regarded here, as if there is a Dirichlet value
               *  for the velocity in face normal direction, it is automatically fixed.
               * \f[
               *    \alpha_\textrm{self}
               *    = - \left( \boldsymbol{\tau} \cdot n \right) \cdot t
               *    = - |\gamma| \varrho \nu_\textrm{t}
               *      \frac{\partial v_\textrm{t}}{\partial x_\textrm{n}}
               * \f]
               * The first tangential case \f$\left( \frac{\partial v_\textrm{t}}{\partial x_\textrm{n}} \right)\f$,
               * is calculated from the velocities of the perpendicular faces of the
               * inside element and the dirichlet value for the normal velocity.<br>
               * Currently no averaging is done for kinematic viscosity and density.
               */
              if(enableNavierStokes_)
              {
                r_s.accumulate(lfsu_v_s, 2*tangDim,
                                -0.5 * density_s * kinematicEddyViscosity_s
                                * (velocityFaces[2*tangDim][tangDim] - dirichletVelocityCorner0[tangDim])
                                  / (insideCellCenterGlobal[normDim] - faceCenterGlobal[normDim])
                                * faceUnitOuterNormal[normDim]
                                * faceVolume);

                r_s.accumulate(lfsu_v_s, 2*tangDim+1,
                                -0.5 * density_s * kinematicEddyViscosity_s
                                * (velocityFaces[2*tangDim+1][tangDim] - dirichletVelocityCorner1[tangDim])
                                  / (insideCellCenterGlobal[normDim] - faceCenterGlobal[normDim])
                                * faceUnitOuterNormal[normDim]
                                * faceVolume);
              }
            }
          }
        }
        // Outflow boundary for velocity
        else if (bcVelocity.isOutflow(ig, faceCenterLocal))
        {
          //! Nothing has to be done here.
        }
        // Symmetry boundary for velocity
        else if (bcVelocity.isSymmetry(ig, faceCenterLocal))
        {
          //! Nothing has to be done in case of Symmetry Condition for velocity.
          //! Ensure normal velocity = 0 is done by the parent function.
        }
        else
        {
          DUNE_THROW(Dune::NotImplemented, "Wrong boundary condition type for velocity.");
        }
      }

      /**
       * \brief Initialize vector-stored values used for alpha_* routines.
       *
       * This function calls the ParentType and additionally
       * Traverses the grid and initialize values, needed for the eddy viscosity models.
       *
       * \tparam GridView GridView type
       */
      void initialize(const GridView& gridView)
      {
        ParentTypeMassMomentum::initialize(gridView);

        if (enableNavierStokes_ == false)
        {
          std::cout << "Navier Stokes is not enabled, but eddy viscosity should be used. This does not make sense!\n";
        }

        typedef typename BC::template Child<velocityIdx>::Type BCVelocity;
        const BCVelocity& bcVelocity = bc.template child<velocityIdx>();

        // velocities and coordinates stored at faces
        storedIntersectionCentersGlobal.resize(mapperIntersection.size());
        storedNeighborFaceCentersGlobal.resize(mapperIntersection.size());
        storedNeighborVelocities.resize(mapperIntersection.size());
        storedVelocityGradientTensor.resize(mapperElement.size());
        storedCorrespondingWallElementID.resize(mapperElement.size());
        storedOppositeWallElementID.resize(mapperElement.size());
        storedCorrespondingWallGlobal.resize(mapperElement.size());
        storedCorrespondingWallNormalAxis.resize(mapperElement.size());
        storedCorrespondingFlowNormalAxis.resize(mapperElement.size());
        storedDistanceToWall.resize(mapperElement.size());
        storedKinematicEddyViscosity.resize(mapperElement.size());
        storedVelocityInWallCoordinates.resize(mapperElement.size());
        storedDistanceInWallCoordinates.resize(mapperElement.size());
        storedRoughness.resize(mapperElement.size());
        storedAdditionalRoughnessLength.resize(mapperElement.size());
        storedVelocityMaximum.resize(mapperElement.size());
        storedVelocityMinimum.resize(mapperElement.size());
        storedNeighborID.resize(mapperElement.size());

        // initialize some of the values
        for (unsigned int i = 0; i < mapperElement.size(); ++i)
        {
          storedNeighborID[i].resize(dim);
          for (unsigned int j = 0; j < dim; ++j)
          {
            storedNeighborID[i][j].resize(2);
            for (unsigned int k = 0; k < 2; ++k)
            {
              storedNeighborID[i][j][k] = 0;
            }
          }
          storedKinematicEddyViscosity[i] = 0.0;
          storedVelocityInWallCoordinates[i] = 0.0;
          storedDistanceInWallCoordinates[i] = 0.0;
          for (unsigned int j = 0; j < dim; ++j)
          {
            storedVelocityMaximum[i][j] = 0.0;
            storedVelocityMinimum[i][j] = 0.0;
          }
          storedRoughness[i] = 0.0;
          storedAdditionalRoughnessLength[i] = 0.0;
        }

        // constants and types
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        typedef typename GridView::IntersectionIterator IntersectionIterator;
        typedef double DF;

        // loop over grid view to get elements with a wall intersection
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          unsigned int intersectionID = 0;
          for (IntersectionIterator is = gridView.ibegin(*eit);
              is != gridView.iend(*eit); ++is)
          {
            typedef typename GridView::template Codim<1>::EntityPointer IntersectionPointer;
            IntersectionPointer intersectionPointer = is->inside()->template subEntity<1>(is->indexInInside());
            const Dune::FieldVector<DF, dim-1>& faceCenterLocal =
              Dune::ReferenceElements<DF, dim-1>::general(is->geometry().type()).position(0, 0);
            Dune::FieldVector<DF, dim> faceCenterGlobal = is->geometry().global(faceCenterLocal);
            storedIntersectionCentersGlobal[mapperIntersection.map(*intersectionPointer)] = faceCenterGlobal;
            ++intersectionID;
          }
        }

        // loop over grid view to get elements with a wall intersection
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
          const Dune::FieldVector<DF, dim>& cellCenterLocal =
            Dune::ReferenceElements<DF, dim>::general(eit->geometry().type()).position(0, 0);
          Dune::FieldVector<DF, dim> cellCenterGlobal = eit->geometry().global(cellCenterLocal);

          // store default case
          for (unsigned int curDim = 0; curDim < dim; ++curDim)
          {
            for (unsigned int curElem = 0; curElem < 2; ++curElem)
            {
              storedNeighborID[elementInsideID][curDim][curElem] = elementInsideID;
            }
          }

          for (IntersectionIterator is = gridView.ibegin(*eit);
              is != gridView.iend(*eit); ++is)
          {

            if (!is->boundary())
            {
              const typename GridView::IndexSet::IndexType neighborElementID = mapperElement.map(*is->outside());
              for (unsigned int curDim = 0; curDim < dim; ++curDim)
              {
                if (std::abs(cellCenterGlobal[curDim]
                             - asImp_().storedElementCentersGlobal[neighborElementID][curDim]) > 1e-7)
                {
                  if (cellCenterGlobal[curDim] > asImp_().storedElementCentersGlobal[neighborElementID][curDim])
                    storedNeighborID[elementInsideID][curDim][0] = neighborElementID;
                  else
                    storedNeighborID[elementInsideID][curDim][1] = neighborElementID;
                }
              }
            }
          }
        }

        // stored velocities are only needed for tangential case of viscous term
        // which is only the case for dim > 1
        if (dim == 1)
        {
          return;
        }

        // vector containing element id's of boundary elements
        std::vector<typename GridView::IndexSet::IndexType> wallElementIDs(0);
        std::vector<Dune::FieldVector<DF, dim>> wallGlobals(0);
        std::vector<unsigned int> wallNormalAxis(0);
        wallElementIDs.clear();
        wallGlobals.clear();
        wallNormalAxis.clear();

        // loop over grid view to get elements with a wall intersection
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
          storedCorrespondingWallGlobal[elementInsideID] = 0.0;

          for (IntersectionIterator is = gridView.ibegin(*eit);
              is != gridView.iend(*eit); ++is)
          {
            const Dune::FieldVector<DF, dim-1>& faceCenterLocal =
              Dune::ReferenceElements<DF, dim-1>::general(is->geometry().type()).position(0, 0);
            Dune::FieldVector<DF, dim> faceCenterGlobal = is->geometry().global(faceCenterLocal);

            if (!bcVelocity.isWall(*is, faceCenterLocal) || !is->boundary())
            {
              continue;
            }

            if (wallNormalAxis_ < 0) // no wallNormalAxis defined (auto-determine)
            {
              for (unsigned int curDim = 0; curDim < dim; ++curDim)
              {
                if (std::abs(is->centerUnitOuterNormal()[curDim]) > 1e-10 && dim > 1)
                {
                  wallElementIDs.push_back(elementInsideID);
                  wallGlobals.push_back(faceCenterGlobal);
                  wallNormalAxis.push_back(curDim);
                }
              }
            }
            else // useWallNormalAxis_
            {
              if (std::abs(is->centerUnitOuterNormal()[wallNormalAxis_]) > 1e-10 && dim > 1)
              {
                wallElementIDs.push_back(elementInsideID);
                wallGlobals.push_back(faceCenterGlobal);
                wallNormalAxis.push_back(wallNormalAxis_);
              }
            }
            break;
          }
        }

        if (wallGlobals.size() == 0 && (wallNormalAxis_ < 0 || flowNormalAxis_ < 0))
        {
          DUNE_THROW(Dune::NotImplemented, "Seems like the problem has no walls and neither wallNormalAxis_ or flowNormalAxis_ axis are set. Setting the values has no influence but leads to segmentation faults and wrong outputs.");
        }

        // loop over grid view
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);

          DF distanceToWall = 1e10;
          Dune::FieldVector<DF, dim> temp;
          for (unsigned int i = 0; i < wallGlobals.size(); ++i)
          {
            temp = eit->geometry().center();
            temp -= wallGlobals[i];
            if (temp.two_norm() < distanceToWall)
            {
              distanceToWall = temp.two_norm();
              storedCorrespondingWallElementID[elementInsideID] = wallElementIDs[i];
              storedCorrespondingWallGlobal[elementInsideID] = wallGlobals[i];
              storedCorrespondingWallNormalAxis[elementInsideID] = wallNormalAxis[i];
            }
          }

          storedDistanceToWall[elementInsideID] = distanceToWall;

          storedOppositeWallElementID[elementInsideID] = elementInsideID;
          for (unsigned int i = 0; i < wallGlobals.size(); ++i)
          {
            temp = eit->geometry().center();
            temp -= wallGlobals[i];
            Scalar otherAxisDifference = 0.0;
            unsigned int normDim = storedCorrespondingWallNormalAxis[elementInsideID];
            for (unsigned int curDim = 0; curDim < dim; ++curDim)
            {
              if (curDim != normDim)
              {
                otherAxisDifference = temp[curDim];
              }
            }

            if (otherAxisDifference < 1e-10
                && temp[normDim] > storedDistanceToWall[elementInsideID])
            {
              storedOppositeWallElementID[elementInsideID] = wallElementIDs[i];
            }
          }

          // store variables for wall functions
          dirichletVelocity.wallNormalAxis_[elementInsideID] = storedCorrespondingWallNormalAxis[elementInsideID];
        }
      }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::updateStoredValues
       */
      template<typename GFS, typename X>
      void updateStoredValues(const GFS& gfs, X& lastSolution)
      {
        ParentTypeMassMomentum::updateStoredValues(gfs, lastSolution);

        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        typedef typename GridView::IntersectionIterator IntersectionIterator;

        // make local function space
        typedef Dune::PDELab::LocalFunctionSpace<GFS> LFS;
        typedef typename LFS::template Child<velocityIdx>::Type LFS_V;
        typedef Dune::PDELab::LFSIndexCache<LFS> LFSCache;
        typedef typename X::template ConstLocalView<LFSCache> XView;

        typedef typename LFS_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits FETraits;
        typedef typename FETraits::DomainFieldType DF;
        typedef typename FETraits::RangeFieldType RF;
        typedef typename FETraits::RangeType RangeType;

        LFS lfs(gfs);
        LFSCache lfsCache(lfs);
        XView xView(lastSolution);
        std::vector<RF> xLocal(lfs.maxSize());
        std::vector<RangeType> basisLocal(lfs.maxSize());

        ///////////
        // loop for tangential storedVelocityGradientTensor
        for (ElementIterator eit = gfs.gridView().template begin<0>();
            eit != gfs.gridView().template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementIdInside = mapperElement.map(*eit);

          // normal case
          for (unsigned int curDim = 0; curDim < dim; ++curDim)
          {
            Dune::FieldVector<DF, dim> faceCenterLocalLeft(0.5), faceCenterLocalRight(0.5);
            faceCenterLocalLeft[curDim] = 0.0;
            faceCenterLocalRight[curDim] = 1.0;

            // bind local function space to element
            lfs.bind(*eit);
            LFS_V lfs_v = lfs.template child<velocityIdx>();
            lfsCache.update();
            xView.bind(lfsCache);
            xView.read(xLocal);
            xView.unbind();

            // evaluate solution bound to grid function space for face mid-points
            RangeType vLeft(0.0), vRight(0.0);
            lfs_v.finiteElement().localBasis().evaluateFunction(faceCenterLocalLeft, basisLocal);
            for (unsigned int i = 0; i < lfs_v.size(); ++i)
            {
              vLeft.axpy(xLocal[i], basisLocal[i]);
            }
            lfs_v.finiteElement().localBasis().evaluateFunction(faceCenterLocalRight, basisLocal);
            for (unsigned int i = 0; i < lfs_v.size(); ++i)
            {
              vRight.axpy(xLocal[i], basisLocal[i]);
            }

            Dune::FieldVector<DF, dim> faceCenterGlobalLeft(0.0), faceCenterGlobalRight(0.0);
            faceCenterGlobalLeft = eit->geometry().global(faceCenterLocalLeft);
            faceCenterGlobalRight = eit->geometry().global(faceCenterLocalRight);

            storedVelocityGradientTensor[elementIdInside][curDim][curDim]
              = (vRight[curDim] - vLeft[curDim]) / (faceCenterGlobalRight[curDim] - faceCenterGlobalLeft[curDim]);
          }
        }

        ///////////
        // loop for tangential storedVelocityGradientTensor
        for (ElementIterator eit = gfs.gridView().template begin<0>();
            eit != gfs.gridView().template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementIdInside = mapperElement.map(*eit);

          // tangential case
          if (dim > 1)
          {
            for (unsigned int vDim = 0; vDim < dim; ++vDim)
            {
              for (unsigned int xDim = 0; xDim < dim; ++xDim)
              {
                if (xDim != vDim)
                {
                  unsigned int leftID = storedNeighborID[elementIdInside][xDim][0];
                  unsigned int rightID = storedNeighborID[elementIdInside][xDim][1];

                  Dune::FieldVector<DF, dim> cellCenterGlobalLeft = asImp_().storedElementCentersGlobal[leftID];
                  Dune::FieldVector<DF, dim> cellCenterGlobalRight = asImp_().storedElementCentersGlobal[rightID];

                  Dune::FieldVector<DF, dim> vLeft = asImp_().storedVelocitiesAtElementCenter[leftID];
                  Dune::FieldVector<DF, dim> vRight = asImp_().storedVelocitiesAtElementCenter[rightID];

                  DF dist = cellCenterGlobalRight[xDim] - cellCenterGlobalLeft[xDim];
                  DF velDiff = vRight[vDim] - vLeft[vDim];

                  storedVelocityGradientTensor[elementIdInside][vDim][xDim] = velDiff / dist;
                }
              }
            }
          }
        }

        ///////////
        // loop for tangential storedVelocityGradientTensor WITH inflow/wall BC
        for (ElementIterator eit = gfs.gridView().template begin<0>();
            eit != gfs.gridView().template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementIdInside = mapperElement.map(*eit);

          for (IntersectionIterator is = gridView.ibegin(*eit);
              is != gridView.iend(*eit); ++is)
          {
            const Dune::FieldVector<DF, dim-1>& faceCenterLocal =
              Dune::ReferenceElements<DF, dim-1>::general(is->geometry().type()).position(0, 0);

            typedef typename BC::template Child<velocityIdx>::Type BCVelocity;
            const BCVelocity& bcVelocity = bc.template child<velocityIdx>();

            //! \todo \bug how to handle symmetry boundary? (should slightly decrease the gradient)
            if (is->boundary()
                && (bcVelocity.isWall(*is, faceCenterLocal) || bcVelocity.isInflow(*is, faceCenterLocal)))
            {
              // evaluate orientation of intersection
              unsigned int normDim = 0;
              for (unsigned int curNormDim = 0; curNormDim < dim; ++curNormDim)
              {
                if (std::abs(is->centerUnitOuterNormal()[curNormDim]) > 1e-10)
                {
                  normDim = curNormDim;
                }
              }

              const Dune::FieldVector<DF, dim>& insideFaceCenterLocal =
                is->geometryInInside().global(faceCenterLocal);
              Dune::FieldVector<DF, dim> faceCenterGlobal =
                is->geometry().global(faceCenterLocal);

              // evaluate boundary condition functions
              typename DirichletVelocity::Traits::RangeType dirichletVelocityCenter;
              dirichletVelocity.evaluate(*eit, insideFaceCenterLocal, dirichletVelocityCenter);

              // update ALL tangential cases
              for (unsigned int vDim = 0; vDim < dim; ++vDim)
              {
                if (normDim != vDim)
                {
                  // left or right ID
                  unsigned int rightID = storedNeighborID[elementIdInside][normDim][0];

                  if (asImp_().storedElementCentersGlobal[elementIdInside][normDim] > faceCenterGlobal[normDim])
                  {
                    rightID = storedNeighborID[elementIdInside][normDim][1];
                  }

                  Dune::FieldVector<DF, dim> cellCenterGlobalRight = asImp_().storedElementCentersGlobal[rightID];
                  Dune::FieldVector<DF, dim> vRight = asImp_().storedVelocitiesAtElementCenter[rightID];
                  DF dist = cellCenterGlobalRight[normDim] - faceCenterGlobal[normDim];
                  DF velDiff = vRight[vDim] - dirichletVelocityCenter[vDim];

                  storedVelocityGradientTensor[elementIdInside][vDim][normDim] = velDiff / dist;
                }
              }
            }
          }
        }

        // loop for storedCorrespondingFlowNormalAxis
        for (ElementIterator eit = gfs.gridView().template begin<0>();
            eit != gfs.gridView().template end<0>(); ++eit)
        {

          if (dim > 1 && flowNormalAxis_ < 0)
          {
            storedCorrespondingFlowNormalAxis[mapperElement.map(*eit)] = 0;
            const typename GridView::IndexSet::IndexType wallElementID = storedCorrespondingWallElementID[mapperElement.map(*eit)];
            unsigned int wallNormalAxis = storedCorrespondingWallNormalAxis[mapperElement.map(*eit)];

            Scalar velocityGradient = 0.0;
            for (unsigned int vDim = 0; vDim < dim; ++vDim)
            {
              if (std::abs(velocityGradient) <
                  std::abs(storedVelocityGradientTensor[wallElementID][vDim][wallNormalAxis]))
              {
                velocityGradient = storedVelocityGradientTensor[wallElementID][vDim][wallNormalAxis];
                storedCorrespondingFlowNormalAxis[mapperElement.map(*eit)] = vDim;
              }
            }
          }
        }

        ///////////
        // loop for normal storedWallValues and dirichletVelocity.Values
        for (ElementIterator eit = gfs.gridView().template begin<0>();
            eit != gfs.gridView().template end<0>(); ++eit)
        {
          if (dim > 1)
          {
            const typename GridView::IndexSet::IndexType wallElementID = storedCorrespondingWallElementID[mapperElement.map(*eit)];
            unsigned int wallNormalAxis = storedCorrespondingWallNormalAxis[mapperElement.map(*eit)];
            unsigned int flowNormalAxis = storedCorrespondingFlowNormalAxis[mapperElement.map(*eit)]; // the flowNormalAxis of the wall decide which is the main flow axis above

            storedVelocityInWallCoordinates[mapperElement.map(*eit)]
              = asImp_().storedVelocitiesAtElementCenter[mapperElement.map(*eit)][flowNormalAxis]
                / std::sqrt(std::abs(storedVelocityGradientTensor[wallElementID][flowNormalAxis][wallNormalAxis])
                            * asImp_().storedKinematicViscosity[wallElementID]);
            storedDistanceInWallCoordinates[mapperElement.map(*eit)]
              = storedDistanceToWall[mapperElement.map(*eit)]
                * std::sqrt(asImp_().storedKinematicViscosity[wallElementID]
                            * std::abs(storedVelocityGradientTensor[wallElementID][flowNormalAxis][wallNormalAxis]))
                / asImp_().storedKinematicViscosity[wallElementID];

            // Store variable for wall functions
            dirichletVelocity.storedVelocityGradientTensor_[mapperElement.map(*eit)] = storedVelocityGradientTensor[mapperElement.map(*eit)];
          }
        }

        // loop for maximum velocities
        for (ElementIterator eit = gfs.gridView().template begin<0>();
            eit != gfs.gridView().template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);

          for (unsigned int j = 0; j < dim; ++j)
          {
            storedVelocityMaximum[elementInsideID][j] = 0.0;
            storedVelocityMinimum[elementInsideID][j] = 999999.9;
          }

          // get maximum and minimum velocities
          for (ElementIterator eit = gfs.gridView().template begin<0>();
              eit != gfs.gridView().template end<0>(); ++eit)
          {
            const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);
            const typename GridView::IndexSet::IndexType wallElementID = storedCorrespondingWallElementID[mapperElement.map(*eit)];

            for (unsigned int curDim = 0; curDim < dim; ++curDim)
            {
              // maximum velocity
              if (storedVelocityMaximum[wallElementID][curDim] < asImp_().storedVelocitiesAtElementCenter[elementInsideID][curDim])
              {
                storedVelocityMaximum[wallElementID][curDim] = asImp_().storedVelocitiesAtElementCenter[elementInsideID][curDim];
              }

              // minimum velocity
              if (storedVelocityMinimum[wallElementID][curDim] > asImp_().storedVelocitiesAtElementCenter[elementInsideID][curDim])
              {
                storedVelocityMinimum[wallElementID][curDim] = asImp_().storedVelocitiesAtElementCenter[elementInsideID][curDim];
              }
            }
          }
        }
      }

private:
      const BC& bc;
      const SourceMomentumBalance& sourceMomentumBalance;
      const SourceMassBalance& sourceMassBalance;
      const DirichletVelocity& dirichletVelocity;
      const DirichletPressure& dirichletPressure;
      const NeumannVelocity& neumannVelocity;
      const NeumannPressure& neumannPressure;
      GridView gridView;

      MapperElement mapperElement;
      MapperIntersection mapperIntersection;

      // properties
      bool enableNavierStokes_;
      bool enableAdvectionAveraging_;
      bool enableDiffusionHarmonic_;
      int wallNormalAxis_;
      int flowNormalAxis_;

protected:
      //! Current implementation.
      Implementation &asImp_()
      { return *static_cast<Implementation*>(this); }
      //! Current implementation.
      const Implementation &asImp_() const
      { return *static_cast<const Implementation*>(this); }
    };
  } // namespace PDELab
} // namespace Dune

#endif // DUMUX_EDDY_VISCOSITY_STAGGERED_GRID_HH
