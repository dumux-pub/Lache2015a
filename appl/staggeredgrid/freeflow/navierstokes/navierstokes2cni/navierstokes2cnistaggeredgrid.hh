/**
 * \file
 * \ingroup StaggeredGrid
 * \ingroup NavierStokes2cniStaggeredGrid
 *
 * \brief Local operator for staggered grid discretization for steady-state
 *        compositional non-isothermal Navier-Stokes equation.
 *
 * \copydoc BaseNavierStokesTransientStaggeredGrid
 * \copydoc BaseComponentStaggeredGrid
 * \copydoc BaseEnergyStaggeredGrid
 */

#ifndef DUMUX_NAVIER_STOKES_TWOCNI_STAGGERED_GRID_DUMUX_HH
#define DUMUX_NAVIER_STOKES_TWOCNI_STAGGERED_GRID_DUMUX_HH

#include<cstddef>

#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/pattern.hh>

#include<appl/staggeredgrid/common/twocomponentfluid.hh>

#include"../navierstokes/basenavierstokesstaggeredgrid.hh"
#include"basecomponentstaggeredgrid.hh"
#include"baseenergystaggeredgrid.hh"

namespace Dune
{
  namespace PDELab
  {
    /**
     * \brief Local operator for staggered grid discretization for steady-state
     *        compositional non-isothermal Navier-Stokes equation.
     *
     * \tparam TypeTag TypeTag of the problem
     */
    template<class TypeTag>
    class NavierStokesTwoCNIStaggeredGrid
    : public NumericalJacobianApplyVolume<NavierStokesTwoCNIStaggeredGrid<TypeTag>>,
      public NumericalJacobianVolume<NavierStokesTwoCNIStaggeredGrid<TypeTag>>,
      public NumericalJacobianSkeleton<NavierStokesTwoCNIStaggeredGrid<TypeTag>>,
      public NumericalJacobianBoundary<NavierStokesTwoCNIStaggeredGrid<TypeTag>>,
      public FullVolumePattern,
      public FullSkeletonPattern,
      public LocalOperatorDefaultFlags,
      public InstationaryLocalOperatorDefaultMethods<>,
      public BaseNavierStokesStaggeredGrid<TypeTag>,
      public BaseComponentStaggeredGrid<TypeTag>,
      public BaseEnergyStaggeredGrid<TypeTag>,
      public Dumux::TwoComponentFluid<TypeTag>
    {
    public:
      typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
      typedef typename GET_PROP_TYPE(TypeTag, LocalOperator) Implementation;

      typedef BaseNavierStokesStaggeredGrid<TypeTag> ParentTypeMassMomentum;
      typedef BaseComponentStaggeredGrid<TypeTag> ParentTypeComponent;
      typedef BaseEnergyStaggeredGrid<TypeTag> ParentTypeEnergy;
      typedef Dumux::TwoComponentFluid<TypeTag> BaseFluid;

      typedef typename GET_PROP_TYPE(TypeTag, BCType) BC;
      typedef typename GET_PROP_TYPE(TypeTag, SourceMomentumBalance) SourceMomentumBalance;
      typedef typename GET_PROP_TYPE(TypeTag, SourceMassBalance) SourceMassBalance;
      typedef typename GET_PROP_TYPE(TypeTag, SourceComponentBalance) SourceComponentBalance;
      typedef typename GET_PROP_TYPE(TypeTag, SourceEnergyBalance) SourceEnergyBalance;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletVelocity) DirichletVelocity;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletPressure) DirichletPressure;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletMassMoleFrac) DirichletMassMoleFrac;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletTemperature) DirichletTemperature;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannVelocity) NeumannVelocity;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannPressure) NeumannPressure;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannMassMoleFrac) NeumannMassMoleFrac;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannTemperature) NeumannTemperature;

      typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
      typedef typename GET_PROP_TYPE(TypeTag, MapperElement) MapperElement;
      typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;
      typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
      enum { dim = GridView::dimension };

      //! \brief Index of unknowns
      typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
      enum { velocityIdx = Indices::velocityIdx,
             pressureIdx = Indices::pressureIdx,
             massMoleFracIdx = Indices::massMoleFracIdx,
             temperatureIdx = Indices::temperatureIdx };

      //! \brief Pattern assembly flags
      enum { doPatternVolume = true,
             doPatternSkeleton = true };

      //! \brief Residual assembly flags
      enum { doAlphaVolume = true,
             doAlphaSkeleton = true,
             doAlphaBoundary = true };

      //! \brief Constructor
      NavierStokesTwoCNIStaggeredGrid(const BC& bc_,
                                      const SourceMomentumBalance& sourceMomentumBalance_, const SourceMassBalance& sourceMassBalance_,
                                      const SourceComponentBalance& sourceComponentBalance_, const SourceEnergyBalance& sourceEnergyBalance_,
                                      const DirichletVelocity& dirichletVelocity_, const DirichletPressure& dirichletPressure_,
                                      const DirichletMassMoleFrac& dirichletMassMoleFrac_, const DirichletTemperature& dirichletTemperature_,
                                      const NeumannVelocity& neumannVelocity_, const NeumannPressure& neumannPressure_,
                                      const NeumannMassMoleFrac& neumannMassMoleFrac_, const NeumannTemperature& neumannTemperature_,
                                      GridView gridView_)
        : ParentTypeMassMomentum(bc_,
              sourceMomentumBalance_, sourceMassBalance_,
              dirichletVelocity_, dirichletPressure_,
              neumannVelocity_, neumannPressure_,
              gridView_),
          ParentTypeComponent(bc_,
              sourceComponentBalance_, dirichletMassMoleFrac_,
              neumannMassMoleFrac_, gridView_),
          ParentTypeEnergy(bc_,
              sourceEnergyBalance_, dirichletTemperature_,
              neumannTemperature_, gridView_),
          bc(bc_),
          sourceMomentumBalance(sourceMomentumBalance_), sourceMassBalance(sourceMassBalance_),
          sourceComponentBalance(sourceComponentBalance_), sourceEnergyBalance(sourceEnergyBalance_),
          dirichletVelocity(dirichletVelocity_), dirichletPressure(dirichletPressure_),
          dirichletMassMoleFrac(dirichletMassMoleFrac_), dirichletTemperature(dirichletTemperature_),
          neumannVelocity(neumannVelocity_), neumannPressure(neumannPressure_),
          neumannMassMoleFrac(neumannMassMoleFrac_), neumannTemperature(neumannTemperature_),
          gridView(gridView_), mapperElement(gridView_)
      {
        useMoles_ = GET_PROP_VALUE(TypeTag, UseMoles);
        initialize(gridView);
      }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_volume
       * \copydoc BaseComponentStaggeredGrid::alpha_volume
       * \copydoc BaseEnergyStaggeredGrid::alpha_volume
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume(const EG& eg, const LFSU& lfsu, const X& x,
                        const LFSV& lfsv, R& r) const
      {
        std::vector<DimVector> velocityFaces = ParentTypeMassMomentum::velocity(eg, lfsu, x);
        Scalar pressure = ParentTypeMassMomentum::pressure(eg, lfsu, x);
        Scalar massMoleFrac = ParentTypeComponent::massMoleFrac(eg, lfsu, x);
        Scalar temperature = ParentTypeEnergy::temperature(eg, lfsu, x);

        ParentTypeMassMomentum::alpha_volume_massmomentum(eg, lfsu, x, lfsv, r,
                                                          velocityFaces, pressure, massMoleFrac, temperature);
        ParentTypeComponent::alpha_volume_component(eg, lfsu, x, lfsv, r,
                                                    velocityFaces, pressure, massMoleFrac, temperature);
        ParentTypeEnergy::alpha_volume_energy(eg, lfsu, x, lfsv, r,
                                              velocityFaces, pressure, massMoleFrac, temperature);
      }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_skeleton
       * \copydoc BaseComponentStaggeredGrid::alpha_skeleton
       * \copydoc BaseEnergyStaggeredGrid::alpha_skeleton
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_skeleton(const IG& ig,
                          const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                          const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                          R& r_s, R& r_n) const
      {
        std::vector<DimVector> velocities_s = ParentTypeMassMomentum::velocity(*(ig.inside()), lfsu_s, x_s);
        std::vector<DimVector> velocities_n = ParentTypeMassMomentum::velocity(*(ig.outside()), lfsu_n, x_n);
        Scalar pressure_s = ParentTypeMassMomentum::pressure(ig.inside(), lfsu_s, x_s);
        Scalar pressure_n = ParentTypeMassMomentum::pressure(ig.outside(), lfsu_n, x_n);
        Scalar massMoleFrac_s = ParentTypeComponent::massMoleFrac(ig.inside(), lfsu_s, x_s);
        Scalar massMoleFrac_n = ParentTypeComponent::massMoleFrac(ig.outside(), lfsu_n, x_n);
        Scalar temperature_s = ParentTypeEnergy::temperature(ig.inside(), lfsu_s, x_s);
        Scalar temperature_n = ParentTypeEnergy::temperature(ig.outside(), lfsu_n, x_n);
        ParentTypeMassMomentum::alpha_skeleton_massmomentum(ig, lfsu_s, x_s, lfsv_s, lfsu_n, x_n, lfsv_n, r_s, r_n,
                                                            velocities_s, pressure_s, massMoleFrac_s, temperature_s,
                                                            velocities_n, pressure_n, massMoleFrac_n, temperature_n);
        ParentTypeComponent::alpha_skeleton_component(ig, lfsu_s, x_s, lfsv_s, lfsu_n, x_n, lfsv_n, r_s, r_n,
                                                      velocities_s, pressure_s, massMoleFrac_s, temperature_s,
                                                      velocities_n, pressure_n, massMoleFrac_n, temperature_n);
        ParentTypeEnergy::alpha_skeleton_energy(ig, lfsu_s, x_s, lfsv_s, lfsu_n, x_n, lfsv_n, r_s, r_n,
                                                velocities_s, pressure_s, massMoleFrac_s, temperature_s,
                                                velocities_n, pressure_n, massMoleFrac_n, temperature_n);
      }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_boundary
       * \copydoc BaseComponentStaggeredGrid::alpha_boundary
       * \copydoc BaseEnergyStaggeredGrid::alpha_boundary
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_boundary(const IG& ig, const LFSU& lfsu_s, const X& x_s,
                          const LFSV& lfsv_s, R& r_s) const
      {
        // boundary face coordinates
        const Dune::FieldVector<Scalar, dim-1>& faceCenterLocal =
          Dune::ReferenceElements<Scalar, dim-1>::general(ig.geometry().type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim>& faceCenterGlobal = ig.geometry().global(faceCenterLocal);

        std::vector<DimVector> velocityFaces = ParentTypeMassMomentum::velocity(*(ig.inside()), lfsu_s, x_s);

        Scalar pressure_s = ParentTypeMassMomentum::pressure(ig.inside(), lfsu_s, x_s);
        typename DirichletPressure::Traits::RangeType pressure_boundary(0.0);
        dirichletPressure.evaluateGlobal(faceCenterGlobal, pressure_boundary);

        Scalar massMoleFrac_s = ParentTypeComponent::massMoleFrac(ig.inside(), lfsu_s, x_s);
        typename DirichletMassMoleFrac::Traits::RangeType massMoleFrac_boundary(0.0);
        dirichletMassMoleFrac.evaluateGlobal(faceCenterGlobal, massMoleFrac_boundary);

        Scalar temperature_s = ParentTypeEnergy::temperature(ig.inside(), lfsu_s, x_s);
        typename DirichletPressure::Traits::RangeType temperature_boundary(0.0);
        dirichletTemperature.evaluateGlobal(faceCenterGlobal, temperature_boundary);

        ParentTypeMassMomentum::alpha_boundary_massmomentum(ig, lfsu_s, x_s, lfsv_s, r_s,
                                                            velocityFaces, pressure_s, massMoleFrac_s, temperature_s,
                                                            pressure_boundary, massMoleFrac_boundary, temperature_boundary);
        ParentTypeComponent::alpha_boundary_component(ig, lfsu_s, x_s, lfsv_s, r_s,
                                                      velocityFaces, pressure_s, massMoleFrac_s, temperature_s,
                                                      pressure_boundary, massMoleFrac_boundary, temperature_boundary);
        ParentTypeEnergy::alpha_boundary_energy(ig, lfsu_s, x_s, lfsv_s, r_s,
                                                velocityFaces, pressure_s, massMoleFrac_s, temperature_s,
                                                pressure_boundary, massMoleFrac_boundary, temperature_boundary);
      }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::initialize
       * \copydoc BaseComponentStaggeredGrid::initialize
       * \copydoc BaseEnergyStaggeredGrid::initialize
       */
      void initialize(const GridView& gridView)
      {
        ParentTypeMassMomentum::initialize(gridView);
        ParentTypeComponent::initialize(gridView);
        ParentTypeEnergy::initialize(gridView);
      }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::updateStoredValues
       * \copydoc BaseComponentStaggeredGrid::updateStoredValues
       * \copydoc BaseEnergyStaggeredGrid::updateStoredValues
       */
      template<typename GFS, typename X>
      void updateStoredValues(const GFS& gfs, X& lastSolution)
      {
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;

        typedef Dune::PDELab::LocalFunctionSpace<GFS> LFS;
        typedef typename LFS::template Child<temperatureIdx>::Type LFS_T;
        typedef typename LFS::template Child<massMoleFracIdx>::Type LFS_C;
        typedef Dune::PDELab::LFSIndexCache<LFS> LFSCache;
        typedef typename X::template ConstLocalView<LFSCache> XView;

        typedef typename LFS_T::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits FETraits_T;
        typedef typename LFS_C::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits FETraits_C;
        typedef typename FETraits_T::RangeType RangeType_t;
        typedef typename FETraits_C::RangeType RangeType_c;

        // loop over grid view
        for (ElementIterator eit = gfs.gridView().template begin<0>();
            eit != gfs.gridView().template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.map(*eit);

          LFS lfs_t(gfs);
          LFSCache lfsCache_t(lfs_t);
          XView xView_t(lastSolution);
          std::vector<Scalar> xLocal_t(lfs_t.maxSize());
          std::vector<RangeType_t> basisLocal_t(lfs_t.maxSize());

          // bind local function space to inside element
          lfs_t.bind(*eit);
          LFS_T lfs_t_s = lfs_t.template child<temperatureIdx>();
          lfsCache_t.update();
          xView_t.bind(lfsCache_t);
          xView_t.read(xLocal_t);
          xView_t.unbind();

          // global and local position of cell and face centers
          Dune::FieldVector<Scalar, dim> cellCenterLocal(0.5);
          Dune::FieldVector<Scalar, dim> cellCenterGlobal = eit->geometry().global(cellCenterLocal);

          // evaluate solution bound to grid function space for face mid-points
          RangeType_t temperature(0.0);
          lfs_t_s.finiteElement().localBasis().evaluateFunction(cellCenterGlobal, basisLocal_t);
          for (unsigned int i = 0; i < lfs_t_s.size(); ++i)
          {
            temperature.axpy(xLocal_t[lfs_t_s.localIndex(i)], basisLocal_t[i]);
          }

          LFS lfs_c(gfs);
          LFSCache lfsCache_c(lfs_c);
          XView xView_c(lastSolution);
          std::vector<Scalar> xLocal_c(lfs_c.maxSize());
          std::vector<RangeType_c> basisLocal_c(lfs_c.maxSize());

          // bind local function space to inside element
          lfs_c.bind(*eit);
          LFS_C lfs_c_s = lfs_c.template child<massMoleFracIdx>();
          lfsCache_c.update();
          xView_c.bind(lfsCache_c);
          xView_c.read(xLocal_c);
          xView_c.unbind();

          // evaluate solution bound to grid function space for face mid-points
          RangeType_c massMoleFrac(0.0);
          lfs_c_s.finiteElement().localBasis().evaluateFunction(cellCenterGlobal, basisLocal_c);
          for (unsigned int i = 0; i < lfs_c_s.size(); ++i)
          {
            massMoleFrac.axpy(xLocal_c[lfs_c_s.localIndex(i)], basisLocal_c[i]);
          }

          asImp_().storedTemperature[elementInsideID] = temperature;
          asImp_().storedMassMoleFrac[elementInsideID] = massMoleFrac;
        }

        ParentTypeMassMomentum::updateStoredValues(gfs, lastSolution);
      }

      /**
       * \brief Writes data of an array with element size into textfile
       *
       * Please use the given script <tt>simulationsOutput.sh</tt> to visualize
       * the data with gnuplot. The pictures can be found in the subfolder
       * <tt>pics</tt> after processing.
       *
       * \param time Current time
       * \param timeStep Number of the current time step
       * \param gridCells Number of grid cells of the problem
       */
      void gnuplotElementOutput(double time, unsigned int timeStep)
      {
        std::stringstream stream("");
        stream << "navierstokesElement-";
        stream << std::setprecision(4) << std::setw(4) << std::setfill('0') << timeStep << ".csv";
        stream.clear();
        std::string string(stream.str());
        const char* fileName = string.c_str();
        std::fstream file;
        file.open(fileName, std::ios::out);
        file << "# time: " << time << std::endl;
        file << "# ";
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          file << "global" << curDim << ",";
        }
        file << "velAtElemCenter[0]" << ",";
        file << "velAtElemCenter[1]" << ",";
        file << "pressure" << ",";
        file << "relativePressure" << ",";
        file << "massMoleFrac" << ",";
        file << "temperature" << ",";
        file << "density" << ",";
        file << "kinematicViscosity" << ",";
        file << "diffusionCoefficient" << ",";
        file << "enthalpyPhase" << ",";
        file << "thermalConductivity" << ",";
        file << std::endl;

        for (unsigned int i = 0; i < asImp_().storedElementCentersGlobal.size(); ++i)
        {
          std::vector<double> min;
          std::vector<double> max;
          min.resize(dim);
          max.resize(dim);

          for (unsigned int i = 0; i < dim; ++i)
          {
            min[i] = 99999.9;
            max[i] = -99999.9;
          }

          for (unsigned int j = 0; j < 2*dim; ++j)
          {
            for (unsigned int curDim = 0; curDim < dim; ++curDim)
            {
              if (min[curDim] > asImp_().storedElementFacesGlobal[i][j][curDim])
                min[curDim] = asImp_().storedElementFacesGlobal[i][j][curDim];
              if (max[curDim] < asImp_().storedElementFacesGlobal[i][j][curDim])
                max[curDim] = asImp_().storedElementFacesGlobal[i][j][curDim];
            }
          }

          const bool cellDataOnly = GET_PARAM_FROM_GROUP(TypeTag, bool, Output, GnuplotCellDataOnly);
          for (unsigned int j = 0; j < 2*dim+1; ++j)
          {
            if (dim == 1)
            {
              if (j == 0 && !cellDataOnly)
              {
                file << min[0] << ",";
              }
              if (j == 1)
              {
                file << asImp_().storedElementCentersGlobal[i][0] << ",";
              }
              if (j == 2 && !cellDataOnly)
              {
                file << max[0] << ",";
              }
            }
            if (dim == 2)
            {
              if (j == 0 && !cellDataOnly)
              {
                file << min[0] << ",";
                file << asImp_().storedElementCentersGlobal[i][1] << ",";
              }
              if (j == 1 && !cellDataOnly)
              {
                file << asImp_().storedElementCentersGlobal[i][0] << ",";
                file << min[1] << ",";
              }
              if (j == 2)
              {
                file << asImp_().storedElementCentersGlobal[i][0] << ",";
                file << asImp_().storedElementCentersGlobal[i][1] << ",";
              }
              if (j == 3 && !cellDataOnly)
              {
                file << max[0] << ",";
                file << asImp_().storedElementCentersGlobal[i][1] << ",";
              }
              if (j == 4 && !cellDataOnly)
              {
                file << asImp_().storedElementCentersGlobal[i][0] << ",";
                file << max[1] << ",";
              }
            }

            if (!cellDataOnly || j == dim)
            {
              file << asImp_().storedVelocitiesAtElementCenter[i][0] << ",";
              file << asImp_().storedVelocitiesAtElementCenter[i][1] << ",";
              file << asImp_().storedPressure[i] << ",";
              file << asImp_().storedPressure[i] - 1e5 << ",";
              file << asImp_().storedMassMoleFrac[i] << ",";
              file << asImp_().storedTemperature[i] << ",";
              file << asImp_().storedDensity[i] << ",";
              file << asImp_().storedKinematicViscosity[i] << ",";
              file << asImp_().storedDiffusionCoefficient[i] << ",";
              file << asImp_().storedEnthalpyPhase[i] << ",";
              file << asImp_().storedThermalConductivity[i] << ",";
              file << std::endl;
            }
          }
        }
        file.close();

        std::ifstream sourceFile(fileName, std::ios::binary);
        std::string string2 = "navierstokes";
        string2.append("Element-current.csv");
        const char* fileName2 = string2.c_str();
        std::ofstream targetFile(fileName2, std::ios::binary);
        targetFile << sourceFile.rdbuf();
        sourceFile.close();
        targetFile.close();
      }

      //! \copydoc BaseNavierStokesStaggeredGrid::gnuplotIntersectionOutput
      void gnuplotIntersectionOutput(double time, unsigned int timeStep)
      {
        ParentTypeMassMomentum::gnuplotIntersectionOutput(time, timeStep);
      }

      //! \brief Returns the density [kg/m^3]
      const Scalar density(Scalar pressure, Scalar temperature, Scalar massMoleFrac) const
      { return BaseFluid::density(pressure, temperature, massMoleFrac); }

      //! \brief Returns the density [mol/m^3]
      const Scalar molarDensity(Scalar pressure, Scalar temperature, Scalar massMoleFrac) const
      { return BaseFluid::molarDensity(pressure, temperature, massMoleFrac); }

      //! \brief Returns the dynamic viscosity [kg/(m s)]
      const Scalar dynamicViscosity(Scalar pressure, Scalar temperature, Scalar massMoleFrac) const
      { return BaseFluid::dynamicViscosity(pressure, temperature, massMoleFrac); }

      //! \brief Returns the kinematic viscosity [m^2/s]
      const Scalar kinematicViscosity(Scalar pressure, Scalar temperature, Scalar massMoleFrac) const
      { return BaseFluid::kinematicViscosity(pressure, temperature, massMoleFrac); }

      //! \brief Returns the thermal conductivity [W/(m*K)]
      const Scalar thermalConductivity(Scalar pressure, Scalar temperature, Scalar massMoleFrac) const
      { return BaseFluid::thermalConductivity(pressure, temperature, massMoleFrac); }

      //! \brief Returns the heat capacity [J/(kg*K)]
      const Scalar heatCapacity(Scalar pressure, Scalar temperature, Scalar massMoleFrac) const
      { return BaseFluid::heatCapacity(pressure, temperature, massMoleFrac); }

      //! \brief Returns the enthalpy of a component [J/kg]
      const Scalar enthalpyComponent(Scalar pressure, Scalar temperature, unsigned int compIdx) const
      { return BaseFluid::enthalpyComponent(pressure, temperature, compIdx); }

      //! \brief Returns the enthalpy of the phase [J/kg]
      const Scalar enthalpyPhase(Scalar pressure, Scalar temperature, Scalar massMoleFrac) const
      { return BaseFluid::enthalpyPhase(pressure, temperature, massMoleFrac); }

      //! \brief Returns the binary diffusion coefficient [m^2/s]
      const Scalar diffusionCoefficient(Scalar pressure, Scalar temperature, Scalar massMoleFrac) const
      { return BaseFluid::diffusionCoefficient(pressure, temperature, massMoleFrac); }

      //! \brief Returns the mass of the component [kg/mol]
      const Scalar molarMassComponent(unsigned int compIdx) const
      { return BaseFluid::molarMassComponent(compIdx); }

      //! \brief Returns the molar mass of the phase [kg/mol]
      const Scalar molarMassPhase(Scalar massMoleFrac) const
      { return BaseFluid::molarMassPhase(massMoleFrac); }

      //! \brief Returns the molar fraction [-]
      const Scalar convertToMoleFrac(Scalar massMoleFrac) const
      { return BaseFluid::convertToMoleFrac(massMoleFrac); }

      /**
       * \brief Returns the eddy diffusivity for a given element [m^2/s]
       *
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       */
      template<typename EG, typename LFSU, typename X>
      const Scalar eddyDiffusivity(const EG& eg, const LFSU& lfsu, const X& x) const
      { return 0.0; }

      /**
       * \brief Returns the eddy diffusivity for a given element [W/(m*K)]
       *
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       */
      template<typename EG, typename LFSU, typename X>
      const Scalar thermalEddyConductivity(const EG& eg, const LFSU& lfsu, const X& x) const
      { return 0.0; }

private:
      const BC& bc;
      const SourceMomentumBalance& sourceMomentumBalance;
      const SourceMassBalance& sourceMassBalance;
      const SourceComponentBalance& sourceComponentBalance;
      const SourceEnergyBalance& sourceEnergyBalance;
      const DirichletVelocity& dirichletVelocity;
      const DirichletPressure& dirichletPressure;
      const DirichletMassMoleFrac& dirichletMassMoleFrac;
      const DirichletTemperature& dirichletTemperature;
      const NeumannVelocity& neumannVelocity;
      const NeumannPressure& neumannPressure;
      const NeumannMassMoleFrac& neumannMassMoleFrac;
      const NeumannTemperature& neumannTemperature;
      GridView gridView;
      MapperElement mapperElement;

      // properties
      bool useMoles_;
protected:
      //! Current implementation.
      Implementation &asImp_()
      { return *static_cast<Implementation*>(this); }
      //! Current implementation.
      const Implementation &asImp_() const
      { return *static_cast<const Implementation*>(this); }

      Problem &problem_()
      { return *problemPtr_; }
      const Problem &problem_() const
      { return *problemPtr_; }

      Problem *problemPtr_;
    };
  } // namespace PDELab
} // namespace Dune

#endif // DUMUX_NAVIER_STOKES_TWOCNI_STAGGERED_GRID_DUMUX_HH
