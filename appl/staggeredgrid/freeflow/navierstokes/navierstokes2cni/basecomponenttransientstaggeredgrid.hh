/**
 * \file
 * \ingroup StaggeredGrid
 * \ingroup ComponentStaggeredGrid
 *
 * \brief Storage term / transient part of staggered grid discretization
 *        for component transport models.
 *
 * The component transport equation for mole fractions:<br>
 *
 * \f[
 *    \frac{\partial}{\partial t}  \left( \varrho_\textrm{mol} x^\kappa \right)
 *    + \dots
 *    = 0
 * \f]
 *
 * The component transport equation for mass fractions:<br>
 *
 * \f[
 *    \frac{\partial}{\partial t}  \left( \varrho X^\kappa \right)
 *    + \dots
 *    = 0
 * \f]
 */

#ifndef DUMUX_BASE_COMPONENT_TRANSIENT_STAGGERED_GRID_HH
#define DUMUX_BASE_COMPONENT_TRANSIENT_STAGGERED_GRID_HH

#include<cstddef>

#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/pattern.hh>

#include"navierstokes2cnipropertydefaults.hh"

namespace Dune
{
  namespace PDELab
  {
    /**
     * \brief Local operator for staggered grid discretization solving
     *        the transient part of the component transport equation.
     *
     * \tparam TypeTag TypeTag of the problem
     */
    template<class TypeTag>
    class BaseComponentTransientStaggeredGrid
    {
    public:
      typedef typename GET_PROP_TYPE(TypeTag, TransientLocalOperator) Implementation;
      typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
      typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

      typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;
      typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

      typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
      enum { massMoleFracIdx = Indices::massMoleFracIdx };

      //! \brief pattern assembly flags
      enum { doPatternVolume = true };

      //! \brief residual assembly flags
      enum { doAlphaVolume = true };

      //! \brief Constructor
      BaseComponentTransientStaggeredGrid(GridView gridView_)
      {
        useMoles_ = GET_PROP_VALUE(TypeTag, UseMoles);
        if (useMoles_)
        {
          DUNE_THROW(NotImplemented, "The transient term is not implemented for mole fraction formulation");
        }
      }

      //! set time for subsequent evaluation
      void setTime (double t)
      {
        time = t;
      }

      /**
       * \copydoc BaseNavierStokesTransientStaggeredGrid::alpha_volume_navierstokes
       * \param density phase density inside the element
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume_component (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r,
                                   std::vector<DimVector> velocityFaces, Scalar pressure,
                                   Scalar massMoleFrac, Scalar temperature) const
      {
        typedef typename LFSU::template Child<massMoleFracIdx>::Type LFSU_C;
        const LFSU_C& lfsu_c = lfsu.template child<massMoleFracIdx>();

        // include molar density if formulated in mole fraction
        const Scalar density = asImp_().density(pressure, temperature, massMoleFrac);
        const Scalar elementVolume = eg.geometry().volume();

        /**
          * (1) \b Storage term of \b component balance equation
          * \f[
          *    \frac{\partial}{\partial t} \varrho X^\kappa
          * \f]
          */
        r.accumulate(lfsu_c, 0,
                     density * massMoleFrac * elementVolume);
      }

      /**
       * \brief Returns the massMoleFrac for a given element
       *
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       */
      template<typename EG, typename LFSU, typename X>
      const Scalar massMoleFrac(const EG& eg, const LFSU& lfsu, const X& x) const
      {
        typedef typename LFSU::template Child<massMoleFracIdx>::Type LFSU_C;
        const LFSU_C& lfsu_c = lfsu.template child<massMoleFracIdx>();
        return x(lfsu_c, 0);
      }

private:
      //! Instationary variables
      double time;

      bool useMoles_;

protected:
      //! Current implementation.
      Implementation &asImp_()
      { return *static_cast<Implementation*>(this); }
      //! Current implementation.
      const Implementation &asImp_() const
      { return *static_cast<const Implementation*>(this); }
    };
  } // end namespace PDELab
} // end namespace Dune

#endif // DUMUX_BASE_COMPONENT_TRANSIENT_STAGGERED_GRID_HH
