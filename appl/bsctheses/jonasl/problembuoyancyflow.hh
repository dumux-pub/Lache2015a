// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for a channel flow using the compositional non-isothermal
 *        kepsilon equation
 */

#ifndef DUMUX_PROBLEM_BUOYANCY_FLOW_HH
#define DUMUX_PROBLEM_BUOYANCY_FLOW_HH

#define USE_CONSTRAINED_PRESSURE 1
#define USE_CONSTRAINED_TEMPERATURE 1

#if NAVIERSTOKES
#include <appl/staggeredgrid/freeflow/navierstokes/navierstokes2cni/navierstokes2cniproblem.hh>
#include <appl/staggeredgrid/freeflow/navierstokes/navierstokes2cni/navierstokes2cnipropertydefaults.hh>
#elif KEPSILON
#include <appl/staggeredgrid/freeflow/twoeq/kepsilon2cni/kepsilon2cniproblem.hh>
#include <appl/staggeredgrid/freeflow/twoeq/kepsilon2cni/kepsilon2cnipropertydefaults.hh>
#elif LOWREKEPSILON
#include <appl/staggeredgrid/freeflow/twoeq/lowrekepsilon2cni/lowrekepsilon2cniproblem.hh>
#include <appl/staggeredgrid/freeflow/twoeq/lowrekepsilon2cni/lowrekepsilon2cnipropertydefaults.hh>
#endif

#include <dumux/io/interfacegridcreator.hh>
#include <dumux/material/fluidsystems/h2oairfluidsystem.hh>

namespace Dumux
{
template <class TypeTag>
class BuoyancyFlowProblem;

namespace Properties
{
#if NAVIERSTOKES
  NEW_TYPE_TAG(BuoyancyFlowProblem, INHERITS_FROM(StaggeredGridNavierStokes2cni));
#elif KEPSILON
  NEW_TYPE_TAG(BuoyancyFlowProblem, INHERITS_FROM(StaggeredGridKEpsilon2cni));
#elif LOWREKEPSILON
  NEW_TYPE_TAG(BuoyancyFlowProblem, INHERITS_FROM(StaggeredGridLowReKEpsilon2cni));
#endif

// Set the problem property
SET_TYPE_PROP(BuoyancyFlowProblem, Problem, Dumux::BuoyancyFlowProblem<TypeTag>);

// Set the used local operator
#if NAVIERSTOKES
  SET_TYPE_PROP(BuoyancyFlowProblem, LocalOperator, Dune::PDELab::NavierStokesTwoCNIStaggeredGrid<TypeTag>);
#elif KEPSILON
  SET_TYPE_PROP(BuoyancyFlowProblem, LocalOperator, Dune::PDELab::KEpsilonTwoCNIStaggeredGrid<TypeTag>);
#elif LOWREKEPSILON
  SET_TYPE_PROP(BuoyancyFlowProblem, LocalOperator, Dune::PDELab::LowReKEpsilonTwoCNIStaggeredGrid<TypeTag>);
#endif

// Set the used transient local operator
#if NAVIERSTOKES
SET_TYPE_PROP(BuoyancyFlowProblem, TransientLocalOperator, Dune::PDELab::NavierStokesTwoCNITransientStaggeredGrid<TypeTag>);
#elif KEPSILON
SET_TYPE_PROP(BuoyancyFlowProblem, TransientLocalOperator, Dune::PDELab::KEpsilonTwoCNITransientStaggeredGrid<TypeTag>);
#elif LOWREKEPSILON
SET_TYPE_PROP(BuoyancyFlowProblem, TransientLocalOperator, Dune::PDELab::LowReKEpsilonTwoCNITransientStaggeredGrid<TypeTag>);
#endif

// Disable gravity field
SET_BOOL_PROP(BuoyancyFlowProblem, ProblemEnableGravity, true);

// Select the fluid system
SET_PROP(BuoyancyFlowProblem, FluidSystem)
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dumux::FluidSystems::H2OAir<Scalar> type;
};

// Set the type of the grid creator
SET_TYPE_PROP(BuoyancyFlowProblem, GridCreator, Dumux::InterfaceGridCreator<TypeTag>);
}

template <class TypeTag>
#if NAVIERSTOKES
  class BuoyancyFlowProblem : public NavierStokesTwoCNIProblem<TypeTag>
#elif KEPSILON
  class BuoyancyFlowProblem : public KEpsilonTwoCNIProblem<TypeTag>
#elif LOWREKEPSILON
  class BuoyancyFlowProblem : public LowReKEpsilonTwoCNIProblem<TypeTag>
#endif
{
#if NAVIERSTOKES
    typedef NavierStokesTwoCNIProblem<TypeTag> ParentType;
#elif KEPSILON
    typedef KEpsilonTwoCNIProblem<TypeTag> ParentType;
#elif LOWREKEPSILON
    typedef LowReKEpsilonTwoCNIProblem<TypeTag> ParentType;
#endif
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    enum { dim = GridView::dimension };

    typedef typename GridView::template Codim<0>::Entity Element;

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;

public:
    BuoyancyFlowProblem(TimeManager &timeManager, const GridView &gridView)
    : ParentType(timeManager, gridView), gridView_(gridView)
    {
        temperatureTop_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, TemperatureTop);
        temperatureBottom_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, TemperatureBottom);
        temperaturePipe_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, TemperaturePipe);
        massMoleFrac_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, MassMoleFrac);
#if (KEPSILON || LOWREKEPSILON)
        turbulentKineticEnergy_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, TurbulentKineticEnergy);
        dissipation_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, Dissipation);
#endif
        FluidSystem::init();
    }

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    {
#if NAVIERSTOKES
        std::string string = "navierstokes";
#elif KEPSILON
        std::string string = "kepsilon";
#elif LOWREKEPSILON
        std::string string = "lowrekepsilon";
#endif
        return string.c_str();
    }

    //! \brief Velocity boundary condition types
    bool bcVelocityIsWall(const DimVector& global) const
    { return this->onLowerBoundary_(global)
             || isPipe(global); }
    bool bcVelocityIsSymmetry(const DimVector& global) const
    { return !bcVelocityIsWall(global); }

    //! \brief Pressure boundary condition types
    bool bcPressureIsDirichlet(const DimVector& global) const
    { return (global[0] < this->bBoxMin()[0] + eps_
              && global[1] < this->bBoxMin()[1] + eps_); }
    bool bcPressureIsOutflow(const DimVector& global) const
    { return !bcPressureIsDirichlet(global); }

    //! \brief MassMoleFrac boundary condition types
    bool bcMassMoleFracIsInflow(const DimVector& global) const
    { return (this->onLowerBoundary_(global) && global[0])
             || (this->onUpperBoundary_(global) && global[0]); }
    bool bcMassMoleFracIsSymmetry(const DimVector& global) const
    { return !bcMassMoleFracIsInflow(global); }

    //! \brief Temperature boundary condition types
    bool bcTemperatureIsInflow(const DimVector& global) const
    { return (this->onLowerBoundary_(global) && global[0])
             || (this->onUpperBoundary_(global) && global[0])
             || isPipe(global); }
    bool bcTemperatureIsOutflow(const DimVector& global) const
    { return false/*this->onUpperBoundary_(global)*/; }
    bool bcTemperatureIsSymmetry(const DimVector& global) const
    { return !bcTemperatureIsInflow(global) && !bcTemperatureIsOutflow(global); }

#if (KEPSILON || LOWREKEPSILON)
    //! \brief TurbulentKineticEnergy boundary condition types
    bool bcTurbulentKineticEnergyIsWall(const DimVector& global) const
    { return this->onLowerBoundary_(global)
             || isPipe(global); }
    bool bcTurbulentKineticEnergyIsInflow(const DimVector& global) const
    { return this->onUpperBoundary_(global); }
    bool bcTurbulentKineticEnergyIsSymmetry(const DimVector& global) const
    { return !bcTurbulentKineticEnergyIsWall(global) && !bcTurbulentKineticEnergyIsInflow(global); }

    //! \brief Dissipation boundary condition types
    bool bcDissipationIsWall(const DimVector& global) const
    { return this->onLowerBoundary_(global)
             || isPipe(global); }
    bool bcDissipationIsInflow(const DimVector& global) const
    { return this->onUpperBoundary_(global); }
    bool bcDissipationIsOutflow(const DimVector& global) const
    { return !bcTurbulentKineticEnergyIsWall(global) && !bcTurbulentKineticEnergyIsInflow(global); }
#endif

    //! \brief Dirichlet values
    DimVector dirichletVelocityAtPos(const Element& e, const DimVector& global) const
    {
        DimVector y(0.0);
        return y;
    }
    Scalar dirichletPressureAtPos(const DimVector& global) const
    { return 1e5; }
    Scalar dirichletMassMoleFracAtPos(const DimVector& global) const
    { return massMoleFrac_; }
    Scalar dirichletTemperatureAtPos(const DimVector& global) const
    {
        if (this->onLowerBoundary_(global))
            return temperatureBottom_;
        if (isPipe(global))
            return temperaturePipe_;
        return temperatureTop_;
    }
#if (KEPSILON || LOWREKEPSILON)
    Scalar dirichletTurbulentKineticEnergyAtPos(const Element& e, const DimVector& global) const
    {
#if KEPSILON
        if (bcTurbulentKineticEnergyIsWall(global)
            && this->timeManager().time() > 0.0)
        {
            return this->dirichletTurbulentKineticEnergy().turbulentKineticEnergyWallFunction(e);
        }
#endif
        return turbulentKineticEnergy_;
    }
    Scalar dirichletDissipationAtPos(const Element& e, const DimVector& global) const
    {
#if KEPSILON
        if (bcTurbulentKineticEnergyIsWall(global)
            && this->timeManager().time() > 0.0)
        {
            return this->dirichletDissipation().dissipationWallFunction(e);
        }
#endif
        return dissipation_;
    }
#endif

    bool isPipe(const DimVector& global) const
    {
        DimVector center(0.0);
        center[0] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePosX);
        center[1] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePosY);
        Scalar radius = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, Radius);

        Scalar sumSquares = 0.0;
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
            sumSquares += std::pow(center[curDim] - global[curDim], 2.0);

        if (std::sqrt(sumSquares) < radius)
            return true;
        return false;
    }

private:
    std::string name_;
    const GridView gridView_;
    static constexpr Scalar eps_ = 1e-6;

    Scalar temperatureTop_;
    Scalar temperatureBottom_;
    Scalar temperaturePipe_;
    Scalar massMoleFrac_;
#if (KEPSILON || LOWREKEPSILON)
    Scalar turbulentKineticEnergy_;
    Scalar dissipation_;
#endif
};

} //end namespace


#endif // DUMUX_PROBLEM_BUOYANCY_FLOW_HH
