// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A grid creator
 */
#include "config.h"

#define NAVIERSTOKES 1

#include <dune/common/precision.hh>
#include "problembuoyancyflow.hh"
#include <dumux/common/start.hh>
#include <appl/staggeredgrid/common/gradeddgfcreator.hh>

/*!
 * \brief Provides an interface for customizing error messages associated with
 *        reading in parameters.
 *
 * \param progName  The name of the program, that was tried to be started.
 * \param errorMsg  The error message that was issued by the start function.
 *                  Comprises the thing that went wrong and a general help message.
 */
void usage(const char *progName, const std::string &errorMsg)
{
    if (errorMsg.size() > 0) {
        std::string errorMessageOut = "\nUsage: ";
                    errorMessageOut += progName;
                    errorMessageOut += " [options]\n";
                    errorMessageOut += errorMsg;
                    errorMessageOut += "\n\nThe List of Mandatory arguments for this program is:\n"
                                        "\t-TimeManager.TEnd          The end of the simulation. [s] \n"
                                        "\t-TimeManager.DtInitial     The initial timestep size. [s] \n"
                                        "\t-Grid.File                 The file name of the file containing the grid \n"
                                        "\t                              definition in DGF format\n";
        std::cout << errorMessageOut << "\n";
    }
}

/*!
 * \brief Creates a grid named "temp.dgf" based on the input parameters
 */
template <class TypeTag>
void createGrid(int argc,
                char **argv)
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

    ////////////////////////////////////////////////////////////
    // Load the input parameters
    ////////////////////////////////////////////////////////////

    typedef typename GET_PROP(TypeTag, ParameterTree) ParameterTree;
    Dune::ParameterTreeParser::readOptions(argc, argv, ParameterTree::tree());

    if (ParameterTree::tree().hasKey("ParameterFile") or argc==1)
    {
        // read input file, but do not overwrite options specified
        // on the command line, since the latter have precedence.
        std::string inputFileName ;
        if(argc==1) // if there are no arguments given (and there is a file ./<programname>.input) we use it as input file
        {
            std::cout<< "\nNo parameter file given. \n"
                     << "Defaulting to '"
                     << argv[0]
                     << ".input' for input file.\n";
            inputFileName = argv[0];
            inputFileName += ".input";
        }
        else
            inputFileName = GET_RUNTIME_PARAM(TypeTag, std::string, ParameterFile); // otherwise we read from the command line

        std::ifstream parameterFile;

        // check whether the parameter file exists.
        parameterFile.open(inputFileName.c_str());
        if (not parameterFile.is_open()){
            std::cout<< "\n\t -> Could not open file"
                     << inputFileName
                     << ". <- \n\n\n\n";
            usage(argv[0], "blabla");
        }
        parameterFile.close();

        Dune::ParameterTreeParser::readINITree(inputFileName,
                                               ParameterTree::tree(),
                                               /*overwrite=*/false);
    }

    Scalar bBoxMinX;
    Scalar bBoxMaxX;
    Scalar bBoxMinY;
    Scalar bBoxMaxY;
    int numberCellsX;
    int numberCellsY;
    Scalar gradingX;
    Scalar gradingY;
    int refinementX;
    int refinementY;

    try
    {
        bBoxMinX = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, LowerLeftX);
        bBoxMaxX = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, UpperRightX);
        bBoxMinY = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, LowerLeftY);
        bBoxMaxY = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, UpperRightY);
        numberCellsX = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Grid, NumberOfCellsX);
        numberCellsY = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Grid, NumberOfCellsY);
        gradingX = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, GradingFactorX);
        gradingY = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, GradingFactorY);
        refinementX = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Grid, RefinementX);
        refinementY = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Grid, RefinementY);
    }
    catch (Dumux::ParameterException &e) {
        std::cerr << e << ". Abort!\n";
        exit(1) ;
    }
    catch (...) {
        std::cerr << "Unknown exception thrown!\n";
        exit(1);
    }

    Dune::FieldVector<Scalar, 2> low(bBoxMinX);
    low[1] = bBoxMinY;
    Dune::FieldVector<Scalar, 2> high(bBoxMaxX);
    high[1] = bBoxMaxY;
    Dune::FieldVector<int, 2> n(numberCellsX);
    n[1] = numberCellsY;
    Dune::FieldVector<Scalar, 2> grading(gradingX);
    grading[1] = gradingY;
    Dune::FieldVector<Scalar, 2> minimumCellHeight(0.0);
    Dune::FieldVector<int, 2> refinementDirection(refinementX);
    refinementDirection[1] = refinementY;
    Dumux::GradedDGFCreator<2> gradeddgfcreator(low, high, n, minimumCellHeight,
                                                grading, refinementDirection);
}

int main(int argc, char** argv)
{
    typedef TTAG(BuoyancyFlowProblem) ProblemTypeTag;
    createGrid<ProblemTypeTag>(argc, argv);
    return 0;
}

