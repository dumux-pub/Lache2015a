if [ "$1" == "-h" -o "$1" == "--help" -o "$1" == "--h" ] || [ $# -lt "3" ]; then
  echo;
  echo "Usage: $0 POSITION VALUE FILES"
  echo; exit
fi

args=("$@")
no=$#

YCOLUMN=`head -n 2 $3 | tail -n 1 | awk -v col=$2 -F"," '{print $col}'`
echo "#Time,"$YCOLUMN > plotOverTime.csv
### To REMOVE FILES MATCHING GIVEN PATTERN
for (( i=2; i<$no; i++ )); do
  PATTERN=${args[${i}]}
  TIME=`awk '{print $3}' $PATTERN`
  VALUE=`grep "$1" $PATTERN | awk -v col=$2 -F"," '{print $col}'`
  echo $TIME","$VALUE >> plotOverTime.csv
done

echo "Creating plot..."
echo " position:  $1"
echo " yValue:    $YCOLUMN"
cp plotOverTime.csv temp.csv

echo "set datafile separator ','" > temp.gp
echo "set title '$YCOLUMN at ($1) over time'" >> temp.gp
echo "set xlabel 'time'" >> temp.gp
echo "set ylabel '$YCOLUMN'" >> temp.gp
echo "plot 'temp.csv' u 1:2 w l" >> temp.gp
echo 'set terminal png size 800,600' >> temp.gp
echo 'set output "plot.png"' >> temp.gp
echo 'replot' >> temp.gp

gnuplot --persist temp.gp
/usr/bin/rm temp.gp temp.csv

exit
