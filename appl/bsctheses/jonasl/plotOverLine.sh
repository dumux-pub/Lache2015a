if [ "$1" == "-h" -o "$1" == "--help" -o "$1" == "--h" ] || [ $# != 4 -a $# != 5 ]; then
  echo;
  echo "Usage: $0 FILE POSITION X_COLUMN Y_COLUMN"
  echo "OPTIONAL:"
  echo "REFERENCE_DATA: laufer, u+y+"
  echo "                mixing_k_x,mixing_e_x,mixing_nut_x,"
  echo "                mixing_k_y,mixing_e_y,mixing_nut_y,"
  echo "                ldcf100_u,ldcf100_v,"
  echo "                ldcf1000_u,ldcf1000_v"
  echo; exit
fi

REFERENCE_DATA_LOCATION=$0
REFERENCE_DATA_LOCATION="${REFERENCE_DATA_LOCATION%/*}"
# echo $REFERENCE_DATA_LOCATION

head -n 2 $1 | tail -n 1 > plotOverLineData.csv
# POSITION=`awk -v col=$2 -F"," '{print $col}' plotOverLineData.csv`
XCOLUMN=`awk -v col=$3 -F"," '{print $col}' plotOverLineData.csv`
YCOLUMN=`awk -v col=$4 -F"," '{print $col}' plotOverLineData.csv`
TIME=`awk '{print $3}' $1`
echo "Creating plot..."
echo " time:      $TIME"
echo " position:  $2"
echo " xValue:    $XCOLUMN"
echo " yValue:    $YCOLUMN"
grep $2 $1 >> plotOverLineData.csv
cp plotOverLineData.csv temp.csv
# sort --key=$4 --field-separator=, plotOverLineData.csv > temp2.csv
# sort --key=$3 --field-separator=, temp2.csv > temp.csv

echo "set datafile separator ','" > temp.gp
echo "set title '$1 at $2 and time $TIME'" >> temp.gp
if [ "$5" == "u+y+" ]; then
  echo "set log x" >> temp.gp
  echo "set xrange [1:3000]" >> temp.gp
  echo "set yrange [0:30]" >> temp.gp
fi
echo "set xlabel '$XCOLUMN'" >> temp.gp
echo "set ylabel '$YCOLUMN'" >> temp.gp
echo "plot 'temp.csv' u "$3":"$4" w l lw 2\\" >> temp.gp
if [ "$5" == "u+y+" ]; then
  echo ", '$REFERENCE_DATA_LOCATION/references/lauferpipe/expData-re50000-u+y+.csv' using 6:7 w p t 'Laufer 1954, Re=50000' \\" >> temp.gp
fi
if [ "$5" == "laufer" ]; then
  echo ", x*(0.2469-x)*4/0.2469/0.2469 w l t 'Stokes, parabolic profile'\\" >> temp.gp
  echo ", '$REFERENCE_DATA_LOCATION/references/lauferpipe/expData-re50000.csv' using (\$6*0.2469):(\$7) w p t 'Laufer 1954, Re=50000' \\" >> temp.gp
  echo ", '$REFERENCE_DATA_LOCATION/references/lauferpipe/staggered-prandtl-re50000-ny32.csv' using 2:9 w l lc 4 t 'Prandtl staggered' \\" >> temp.gp
  echo ", '$REFERENCE_DATA_LOCATION/references/lauferpipe/box-prandtl-re50000-ny32.csv' using 2:7 w l lc 4 t 'Prandtl box' \\" >> temp.gp
  echo ", '$REFERENCE_DATA_LOCATION/references/lauferpipe/staggered-modVanDriest-re50000-ny32.csv' using 2:9 w l lc 5 t 'Van-Driest staggered' \\" >> temp.gp
  echo ", '$REFERENCE_DATA_LOCATION/references/lauferpipe/box-vanDriest-re50000-ny32.csv' using 2:7 w l lc 5 t 'Van-Driest box' \\" >> temp.gp
  echo ", '$REFERENCE_DATA_LOCATION/references/lauferpipe/staggered-cebeci-re50000-ny32.csv' using 2:9 w l lc 9 t 'Cebeci Smith staggered' \\" >> temp.gp
  echo ", '$REFERENCE_DATA_LOCATION/references/lauferpipe/staggered-baldwinLomax-re50000-ny32.csv' using 2:9 w l lc 7 t 'Baldwin Lomax staggered' \\" >> temp.gp
  echo ", '$REFERENCE_DATA_LOCATION/references/lauferpipe/box-baldwinLomax-re50000-ny32.csv' using 2:7 w l lc 7 t 'Baldwin Lomax box' \\" >> temp.gp
  echo ", '$REFERENCE_DATA_LOCATION/references/lauferpipe/staggered-spalartallmaras-re50000-ny32.csv' using 2:7 w l t 'Spalart Allmaras staggered' \\" >> temp.gp
  echo ", '$REFERENCE_DATA_LOCATION/references/lauferpipe/staggered-kepsilon-re50000-ny32.csv' using 2:7 w l t 'k-epsilon staggered' \\" >> temp.gp
  echo ", '$REFERENCE_DATA_LOCATION/references/lauferpipe/staggered-lowrekepsilon-re50000-ny32.csv' using 2:7 w l t 'low-Re k-epsilon staggered' \\" >> temp.gp
fi
if [ "$5" == "mixing_k_x" ]; then
  echo ", '$REFERENCE_DATA_LOCATION/references/mixinglayer/openfoam_y0.csv' using 11:3 w l t 'OpenFoam, k' \\" >> temp.gp
  echo ", '$REFERENCE_DATA_LOCATION/references/mixinglayer/staggered_y0.csv' using 1:8 w l t 'staggered reference, k @y=0.0075' \\" >> temp.gp
  echo ", '$REFERENCE_DATA_LOCATION/references/mixinglayer/dumux_y0.csv' using 12:8 w l t 'dumux reference, k' \\" >> temp.gp
fi
if [ "$5" == "mixing_k_y" ]; then
  echo ", '$REFERENCE_DATA_LOCATION/references/mixinglayer/openfoam_x1.0.csv' using 12:3 w l t 'OpenFoam, epsilon' \\" >> temp.gp
  echo ", '$REFERENCE_DATA_LOCATION/references/mixinglayer/staggered_x1.0.csv' using 2:8 w l t 'staggered reference, k @x=1.06' \\" >> temp.gp
  echo ", '$REFERENCE_DATA_LOCATION/references/mixinglayer/dumux_x1.0.csv' using 13:8 w l t 'dumux reference, epsilon' \\" >> temp.gp
fi
if [ "$5" == "mixing_e_x" ]; then
  echo ", '$REFERENCE_DATA_LOCATION/references/mixinglayer/openfoam_y0.csv' using 11:4 w l t 'OpenFoam, epsilon' \\" >> temp.gp
  echo ", '$REFERENCE_DATA_LOCATION/references/mixinglayer/staggered_y0.csv' using 1:9 w l t 'staggered reference, epsilon @y=0.0075' \\" >> temp.gp
  echo ", '$REFERENCE_DATA_LOCATION/references/mixinglayer/dumux_y0.csv' using 12:9 w l t 'dumux reference, epsilon' \\" >> temp.gp
fi
if [ "$5" == "mixing_e_y" ]; then
  echo ", '$REFERENCE_DATA_LOCATION/references/mixinglayer/openfoam_x1.0.csv' using 12:4 w l t 'OpenFoam, epsilon' \\" >> temp.gp
  echo ", '$REFERENCE_DATA_LOCATION/references/mixinglayer/staggered_x1.0.csv' using 2:9 w l t 'staggered reference, epsilon @x=1.06' \\" >> temp.gp
  echo ", '$REFERENCE_DATA_LOCATION/references/mixinglayer/dumux_x1.0.csv' using 13:9 w l t 'dumux reference, epsilon' \\" >> temp.gp
fi
if [ "$5" == "mixing_nut_x" ]; then
  echo ", '$REFERENCE_DATA_LOCATION/references/mixinglayer/openfoam_y0.csv' using 11:2 w l t 'OpenFoam, nu_t' \\" >> temp.gp
  echo ", '$REFERENCE_DATA_LOCATION/references/mixinglayer/staggered_y0.csv' using 1:3 w l t 'staggered reference, nu_t @y=0.0075' \\" >> temp.gp
  echo ", '$REFERENCE_DATA_LOCATION/references/mixinglayer/dumux_y0.csv' using 12:(0.09*\$8*\$8/\$9) w l t 'dumux reference, nu_t' \\" >> temp.gp
fi
if [ "$5" == "mixing_nut_y" ]; then
  echo ", '$REFERENCE_DATA_LOCATION/references/mixinglayer/openfoam_x1.0.csv' using 12:2 w l t 'OpenFoam, nu_t' \\" >> temp.gp
  echo ", '$REFERENCE_DATA_LOCATION/references/mixinglayer/staggered_x1.0.csv' using 2:3 w l t 'staggered reference, nu_t @x=1.06' \\" >> temp.gp
  echo ", '$REFERENCE_DATA_LOCATION/references/mixinglayer/dumux_x1.0.csv' using 13:(0.09*\$8*\$8/\$9) w l t 'dumux reference, nu_t' \\" >> temp.gp
fi
if [ "$5" == "ldcf1000_u" ]; then
  echo ", '$REFERENCE_DATA_LOCATION/references/liddrivencavityflow/ghiaU.csv' using 5:2 w p t 'Ghia, u' \\" >> temp.gp
  echo ", '$REFERENCE_DATA_LOCATION/references/liddrivencavityflow/staggeredgrid-re1000U.csv' using 2:6 w l t 'staggered reference, u' \\" >> temp.gp
fi
if [ "$5" == "ldcf1000_v" ]; then
  echo ", '$REFERENCE_DATA_LOCATION/references/liddrivencavityflow/ghiaV.csv' using 2:5 w p t 'Ghia, v' \\" >> temp.gp
  echo ", '$REFERENCE_DATA_LOCATION/references/liddrivencavityflow/staggeredgrid-re1000V.csv' using 6:3 w l t 'staggered reference, v' \\" >> temp.gp
fi
echo " " >> temp.gp
echo 'set terminal png size 800,600' >> temp.gp
echo 'set output "plot.png"' >> temp.gp
echo 'replot' >> temp.gp

gnuplot --persist temp.gp
/usr/bin/rm temp.gp temp.csv

exit
