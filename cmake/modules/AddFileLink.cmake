# for convienence just uses the dune/dumux infrastructure
macro(add_file_link file_name)
  dune_symlink_to_source_files(FILES ${file_name})
endmacro()
