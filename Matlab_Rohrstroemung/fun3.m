

function F=fun3(x)
v=0.5;      %Einlassgeschwindigkeit [m/s]
d=0.026;    %kleiner Durchmesser [m]
D=0.066;    %großer Durchmesser [m]
r=0.013;    %Rauhigkeitsbeiwert (Lambda) von d
R=0.011;    %Rauhigkeitsbeiwert (Lambda) von D
g2=19.62;    %2*g
l=135;       %Länge Zeilenleitung
L=2;         %Läne Zuleitung
F=[d^2/4*pi*x(1)+D^2/4*pi*x(2)-v*D^2/4*pi;
   d^2/4*pi*x(3)+d^2/4*pi*x(4)-D^2/4*pi*x(2);
   r*l/d*x(3)^2/g2+R*9/D*x(2)^2/g2*2-r*l/d*x(1)^2/g2+5*x(3)^2/g2+7*x(2)^2/g2+0.5*v^2/g2-3*x(1)^2/g2-7*v^2/g2;
   r*(l+2*L)/d*x(4)^2/g2+R*9/D*x(2)^2/g2*2-r*l/d*x(1)^2/g2+0.7*x(2)^2/g2+0.11*x(4)^2/g2*2-3*x(1)^2/g2-7*v^2/g2];
end


%  x0 = [1; 0.5; 1; 1];   %Make a starting guess at the solution
%  options = optimoptions('fsolve','Display','iter'); %Option to display output
%  [x,fval] = fsolve(@myfun,x0,options)  %Call solver
%oder:
%[X,FVAL,EXITFLAG,OUTPUT,JACOB] = fsolve(@fun,x0)

%ohne örtliche Verluste:
%X =
%
 %   1.0976
  %  0.3297
   % 1.0954
    %1.0289
   
    
%    Mit:
 %   X =

  %  1.0901
   % 0.3308
    %1.0784
    %1.0534
