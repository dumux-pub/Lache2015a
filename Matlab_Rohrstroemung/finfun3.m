%VERLUSTHÖHE ANLAGE bis zum Punkt x im Rohr 1 mit v1 bei l=y m

function F=finfun3(x)
v=1;      %Einlassgeschwindigkeit [m/s]
v1=0.666;  %Fließgeschw. erstes Parallelrohr
d=0.026;    %kleiner Durchmesser [m]
D=0.066;    %großer Durchmesser [m]
r=0.013;    %Rauhigkeitsbeiwert (Lambda) von d
R=0.011;    %Rauhigkeitsbeiwert (Lambda) von D
g2=19.62;    %2*g
l=135/2;       %Länge Zeilenleitung
L=150/2;         %Länge Zu- und Ableitung
a=1.3;      %Verlustbeiwert des abzweigenden Rohres
b=-0.2;     %Verlustbeiwert des vereinigenden Rohres
%c=0.2;      %Verlustbeiwert des vereinigten Hauptrohres
%d=0.0;      %Verlustbeiwert des abgezweigten Hauptrohres
e=0.4;      %Drosselklappe, voll offen (Hydromech im Wasserbau S.178
f=0.11;     %90°Kurve
h=0.5;      %Ein-/Auslass
i=0.2;        %Rückschlagventil
j=0.05;     %Kugelhahnventil, ganz offen, 2,5'


F=[r*l/d*v1^2/g2+R*L/D*v^2/g2+a*v1^2/g2+e*v^2/g2+f*v^2/g2+h*v^2/g2+j*v^2/g2+v^2/g2-x];
end


%  x0 = [5];   %geschätzte Startwerte
%  options = optimoptions('fsolve','Display','iter'); %Option to display output
%  [x,fval] = fsolve(@finfun,x0,options)  %Call solver
%oder:
%[X,FVAL,EXITFLAG,OUTPUT,JACOB] = fsolve(@finfun2,x0)

% X=gesamte Verlusthöhe OHNE Ausflussgeschwindigkeit=
% 
% 2,88m bei v=1, v1=0,666   gleiches v MIT Ausfl.-geschw.: 2.9326m mit
% Ventilen: 3,06
% 72.0279m  v=5, v1=3,3295

%genau die Mitte: x =   1.5345  --> p=1.147bar