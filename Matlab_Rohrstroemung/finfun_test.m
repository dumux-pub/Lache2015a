

function F=finfun_test(x)
v=1;      %Einlassgeschwindigkeit [m/s]
d=0.026;    %kleiner Durchmesser [m]
D=0.066;    %großer Durchmesser [m]
r=0.013;    %Rauhigkeitsbeiwert (Lambda) von d
R=0.011;    %Rauhigkeitsbeiwert (Lambda) von D
g2=19.62;    %2*g
l=135;       %Länge Zeilenleitung
L=4;         %Länge Zu- und Ableitung
a=1.3;      %Verlustbeiwert des abzweigenden Rohres
b=-0.2;     %Verlustbeiwert des vereinigenden Rohres
c=0.2;      %Verlustbeiwert des vereinigten Hauptrohres
%c=0.0;      %Verlustbeiwert des abgezweigten Hauptrohres

% Q = v*D^2/4*pi;
% Q1 = d^2/4*pi*x(1);
% Q2 = d^2/4*pi*x(2);
% Q3 = d^2/4*pi*x(3);
% Q4 = d^2/4*pi*x(4);
% Q5 = d^2/4*pi*x(6);
% Q6 = d^2/4*pi*x(6);
% Q7 = d^2/4*pi*x(7);
% Q8 = d^2/4*pi*x(8);
% Q9 = d^2/4*pi*x(9);
% Q10 = d^2/4*pi*x(10);
% Q11 = D^2/4*pi*x(11);
% Q12 = D^2/4*pi*x(12);
% Q13 = D^2/4*pi*x(13);
% Q14 = D^2/4*pi*x(14);
% Q15 = D^2/4*pi*x(16);
% Q16 = D^2/4*pi*x(16);
% Q17 = D^2/4*pi*x(17);
% Q18 = D^2/4*pi*x1(8);
% Q19 = D^2/4*pi*x(19);

F1 = [d^2/4*pi*x(1)+D^2/4*pi*x(11)-v*D^2/4*pi]
F2 = [d^2/4*pi*x(2)+D^2/4*pi*x(12)-D^2/4*pi*x(11)]
F3 = [d^2/4*pi*x(3)+D^2/4*pi*x(13)-D^2/4*pi*x(12)]
F4 = [d^2/4*pi*x(4)+D^2/4*pi*x(14)-D^2/4*pi*x(13)]
F5 = [d^2/4*pi*x(5)+D^2/4*pi*x(15)-D^2/4*pi*x(14)]
F6 = [d^2/4*pi*x(6)+D^2/4*pi*x(16)-D^2/4*pi*x(15)]
F7 = [d^2/4*pi*x(7)+D^2/4*pi*x(17)-D^2/4*pi*x(16)]
F8 = [d^2/4*pi*x(8)+D^2/4*pi*x(18)-D^2/4*pi*x(17)]
F9 = [d^2/4*pi*x(9)+D^2/4*pi*x(19)-D^2/4*pi*x(18)]
F10 = [d^2/4*pi*x(10)-D^2/4*pi*x(19)]
F11 = [r*l/d*x(2)^2/g2+R*L/D*x(11)^2/g2-r*l/d*x(1)^2/g2+a*x(2)^2/g2+b*x(11)^2/g2+c*v^2/g2-a*x(1)^2/g2-b*v^2/g2]
F12 = [r*l/d*x(3)^2/g2+R*L/D*x(11)^2/g2+R*L/D*x(12)^2/g2-r*l/d*x(1)^2/g2+a*x(3)^2/g2+b*x(12)^2/g2+c*v^2/g2+c*x(11)^2/g2-a*x(1)^2/g2-b*v^2/g2]
F13 = [r*l/d*x(4)^2/g2+R*L/D*x(11)^2/g2+R*L/D*x(12)^2/g2+R*L/D*x(13)^2/g2-r*l/d*x(1)^2/g2+a*x(4)^2/g2+b*x(13)^2/g2+c*v^2/g2+c*x(11)^2/g2+c*x(12)^2/g2-a*x(1)^2/g2-b*v^2/g2]
F14 = [r*l/d*x(5)^2/g2+R*L/D*x(11)^2/g2+R*L/D*x(12)^2/g2+R*L/D*x(13)^2/g2+R*L/D*x(14)^2/g2-r*l/d*x(1)^2/g2+a*x(5)^2/g2+b*x(14)^2/g2+c*v^2/g2+c*x(11)^2/g2+c*x(12)^2/g2+c*x(13)^2/g2-a*x(1)^2/g2-b*v^2/g2]
F15 = [r*l/d*x(6)^2/g2+R*L/D*x(11)^2/g2+R*L/D*x(12)^2/g2+R*L/D*x(13)^2/g2+R*L/D*x(14)^2/g2+R*L/D*x(15)^2/g2-r*l/d*x(1)^2/g2+a*x(6)^2/g2+b*x(15)^2/g2+c*v^2/g2+c*x(11)^2/g2+c*x(12)^2/g2+c*x(13)^2/g2+c*x(14)^2/g2-a*x(1)^2/g2-b*v^2/g2]
F16 = [r*l/d*x(7)^2/g2+R*L/D*x(11)^2/g2+R*L/D*x(12)^2/g2+R*L/D*x(13)^2/g2+R*L/D*x(14)^2/g2+R*L/D*x(15)^2/g2+R*L/D*x(16)^2/g2-r*l/d*x(1)^2/g2+a*x(7)^2/g2+b*x(16)^2/g2+c*v^2/g2+c*x(11)^2/g2+c*x(12)^2/g2+c*x(13)^2/g2+c*x(14)^2/g2+c*x(15)^2/g2-a*x(1)^2/g2-b*v^2/g2]
F17 = [r*l/d*x(8)^2/g2+R*L/D*x(11)^2/g2+R*L/D*x(12)^2/g2+R*L/D*x(13)^2/g2+R*L/D*x(14)^2/g2+R*L/D*x(15)^2/g2+R*L/D*x(16)^2/g2+R*L/D*x(17)^2/g2-r*l/d*x(1)^2/g2+a*x(8)^2/g2+b*x(17)^2/g2+c*v^2/g2+c*x(11)^2/g2+c*x(12)^2/g2+c*x(13)^2/g2+c*x(14)^2/g2+c*x(15)^2/g2+c*x(16)^2/g2-a*x(1)^2/g2-b*v^2/g2]
F18 = [r*l/d*x(9)^2/g2+R*L/D*x(11)^2/g2+R*L/D*x(12)^2/g2+R*L/D*x(13)^2/g2+R*L/D*x(14)^2/g2+R*L/D*x(15)^2/g2+R*L/D*x(16)^2/g2+R*L/D*x(17)^2/g2+R*L/D*x(18)^2/g2-r*l/d*x(1)^2/g2+a*x(9)^2/g2+b*x(18)^2/g2+c*v^2/g2+c*x(11)^2/g2+c*x(12)^2/g2+c*x(13)^2/g2+c*x(14)^2/g2+c*x(15)^2/g2+c*x(16)^2/g2+c*x(17)^2/g2-a*x(1)^2/g2-b*v^2/g2]
F19 = [r*l/d*x(10)^2/g2+R*L/D*x(11)^2/g2+R*L/D*x(12)^2/g2+R*L/D*x(13)^2/g2+R*L/D*x(14)^2/g2+R*L/D*x(15)^2/g2+R*L/D*x(16)^2/g2+R*L/D*x(17)^2/g2+R*L/D*x(18)^2/g2+R*L/D*x(19)^2/g2-r*l/d*x(1)^2/g2+0.2*x(10)^2/g2+0.2*x(19)^2/g2+c*v^2/g2+c*x(11)^2/g2+c*x(12)^2/g2+c*x(13)^2/g2+c*x(14)^2/g2+c*x(15)^2/g2+c*x(16)^2/g2+c*x(17)^2/g2+c*x(18)^2/g2-a*x(1)^2/g2-b*v^2/g2]

F=[F1;F2;F3;F4;F5;F6;F7;F8;F9;F10;F11;F12;F13;F14;F15;F16;F17;F18;F19]
% F=[d^2/4*pi*x(1)+D^2/4*pi*x(11)-v*D^2/4*pi;
%    d^2/4*pi*x(2)+D^2/4*pi*x(12)-D^2/4*pi*x(11);
%    d^2/4*pi*x(3)+D^2/4*pi*x(13)-D^2/4*pi*x(12);
%    d^2/4*pi*x(4)+D^2/4*pi*x(14)-D^2/4*pi*x(13);
%    d^2/4*pi*x(5)+D^2/4*pi*x(15)-D^2/4*pi*x(14);
%    d^2/4*pi*x(6)+D^2/4*pi*x(16)-D^2/4*pi*x(15);
%    d^2/4*pi*x(7)+D^2/4*pi*x(17)-D^2/4*pi*x(16);
%    d^2/4*pi*x(8)+D^2/4*pi*x(18)-D^2/4*pi*x(17);
%    d^2/4*pi*x(9)+D^2/4*pi*x(19)-D^2/4*pi*x(18);
%    d^2/4*pi*x(10)-D^2/4*pi*x(19);
%    r*l/d*x(2)^2/g2+R*L/D*x(11)^2/g2-r*l/d*x(1)^2/g2+a*x(2)^2/g2+b*x(11)^2/g2+c*v^2/g2                                   -a*x(1)^2/g2-b*v^2/g2;
%    r*l/d*x(3)^2/g2+R*L/D*x(11)^2/g2+R*L/D*x(12)^2/g2-r*l/d*x(1)^2/g2+a*x(3)^2/g2+b*x(12)^2/g2+c*v^2/g2+c*x(11)^2/g2                                                                               -a*x(1)^2/g2-b*v^2/g2;
%    r*l/d*x(4)^2/g2+R*L/D*x(11)^2/g2+R*L/D*x(12)^2/g2+R*L/D*x(13)^2/g2-r*l/d*x(1)^2/g2+a*x(4)^2/g2+b*x(13)^2/g2+c*v^2/g2+c*x(11)^2/g2+c*x(12)^2/g2                                                                  -a*x(1)^2/g2-b*v^2/g2;
%    r*l/d*x(5)^2/g2+R*L/D*x(11)^2/g2+R*L/D*x(12)^2/g2+R*L/D*x(13)^2/g2+R*L/D*x(14)^2/g2-r*l/d*x(1)^2/g2+a*x(5)^2/g2+b*x(14)^2/g2+c*v^2/g2+c*x(11)^2/g2+c*x(12)^2/g2+c*x(13)^2/g2                                                     -a*x(1)^2/g2-b*v^2/g2;
%    r*l/d*x(6)^2/g2+R*L/D*x(11)^2/g2+R*L/D*x(12)^2/g2+R*L/D*x(13)^2/g2+R*L/D*x(14)^2/g2+R*L/D*x(15)^2/g2-r*l/d*x(1)^2/g2+a*x(6)^2/g2+b*x(15)^2/g2+c*v^2/g2+c*x(11)^2/g2+c*x(12)^2/g2+c*x(13)^2/g2+c*x(14)^2/g2                                        -a*x(1)^2/g2-b*v^2/g2;
%    r*l/d*x(7)^2/g2+R*L/D*x(11)^2/g2+R*L/D*x(12)^2/g2+R*L/D*x(13)^2/g2+R*L/D*x(14)^2/g2+R*L/D*x(15)^2/g2+R*L/D*x(16)^2/g2-r*l/d*x(1)^2/g2+a*x(7)^2/g2+b*x(16)^2/g2+c*v^2/g2+c*x(11)^2/g2+c*x(12)^2/g2+c*x(13)^2/g2+c*x(14)^2/g2+c*x(15)^2/g2                           -a*x(1)^2/g2-b*v^2/g2;
%    r*l/d*x(8)^2/g2+R*L/D*x(11)^2/g2+R*L/D*x(12)^2/g2+R*L/D*x(13)^2/g2+R*L/D*x(14)^2/g2+R*L/D*x(15)^2/g2+R*L/D*x(16)^2/g2+R*L/D*x(17)^2/g2-r*l/d*x(1)^2/g2+a*x(8)^2/g2+b*x(17)^2/g2+c*v^2/g2+c*x(11)^2/g2+c*x(12)^2/g2+c*x(13)^2/g2+c*x(14)^2/g2+c*x(15)^2/g2+c*x(16)^2/g2-a*x(1)^2/g2-b*v^2/g2;
%    r*l/d*x(9)^2/g2+R*L/D*x(11)^2/g2+R*L/D*x(12)^2/g2+R*L/D*x(13)^2/g2+R*L/D*x(14)^2/g2+R*L/D*x(15)^2/g2+R*L/D*x(16)^2/g2+R*L/D*x(17)^2/g2+R*L/D*x(18)^2/g2-r*l/d*x(1)^2/g2+a*x(9)^2/g2+b*x(18)^2/g2+c*v^2/g2+c*x(11)^2/g2+c*x(12)^2/g2+c*x(13)^2/g2+c*x(14)^2/g2+c*x(15)^2/g2+c*x(16)^2/g2+c*x(17)^2/g2 -a*x(1)^2/g2-b*v^2/g2;
% %    r*l/d*x(10)^2/g2+R*L/D*x(11)^2/g2+R*L/D*x(12)^2/g2+R*L/D*x(13)^2/g2+R*L/D*x(14)^2/g2+R*L/D*x(15)^2/g2+R*L/D*x(16)^2/g2+R*L/D*x(17)^2/g2+R*L/D*x(18)^2/g2+R*L/D*x(19)^2/g2];
%    r*l/d*x(10)^2/g2+R*L/D*x(11)^2/g2+R*L/D*x(12)^2/g2+R*L/D*x(13)^2/g2+R*L/D*x(14)^2/g2+R*L/D*x(15)^2/g2+R*L/D*x(16)^2/g2+R*L/D*x(17)^2/g2+R*L/D*x(18)^2/g2+R*L/D*x(19)^2/g2-r*l/d*x(1)^2/g2+0.2*x(10)^2/g2+0.2*x(19)^2/g2+c*v^2/g2+c*x(11)^2/g2+c*x(12)^2/g2+c*x(13)^2/g2+c*x(14)^2/g2+c*x(15)^2/g2+c*x(16)^2/g2+c*x(17)^2/g2+c*x(18)^2/g2 -a*x(1)^2/g2-b*v^2/g2];
end


%  x0 = [1; 1; 1; 1; 1; 1; 1; 1; 1; 1; 0.9; 0.8; 0.7; 0.6; 0.5; 0.4; 0.3; 0.2; 0.1];   %geschätzte Startwerte
%  options = optimoptions('fsolve','Display','iter'); %Option to display output
%  [x,fval] = fsolve(@finfun,x0,options)  %Call solver
%oder:
%[X,FVAL,EXITFLAG,OUTPUT,JACOB] = fsolve(@finfun,x0)

%ohne örtliche Verluste:
%X =
%
 %   1.0976
  %  0.3297
   % 1.0954
    %1.0289
   
    
%    Mit:
 %   X =

  %  1.0901
   % 0.3308
    %1.0784
    %1.0534
