

function F=fun2(x)
v=0.5;        %Einlassgeschwindigkeit [m/s]
d=0.026;    %kleiner Durchmesser [m]
D=0.066;    %großer Durchmesser [m]
l=0.013;    %Lambda von d
L=0.011;    %Lambda von D
F=[d^2/4*pi*x(1)+D^2/4*pi*x(2)-v*D^2/4*pi;
   d^2/4*pi*x(3)+d^2/4*pi*x(4)-D^2/4*pi*x(2);
   l*135/d*x(3)^2/2/9.81+L*9/D*x(2)^2/2/9.81*2-l*135/d*x(1)^2/2/9.81+5*x(3)^2/2/9.81+7*x(2)^2/2/9.81+0.5*v^2/2/9.81-3*x(1)^2/2/9.81-7*v^2/2/9.81;
   l*153/d*x(4)^2/2/9.81+L*9/D*x(2)^2/2/9.81*2-l*135/d*x(1)^2/2/9.81+0.7*x(2)^2/2/9.81+0.11*x(4)^2/2/9.81*2-3*x(1)^2/2/9.81-7*v^2/2/9.81];
end


%  x0 = [1; 0.5; 1; 1];   %Make a starting guess at the solution
%  options = optimoptions('fsolve','Display','iter'); %Option to display output
%  [x,fval] = fsolve(@myfun,x0,options)  %Call solver
%oder:
%[X,FVAL,EXITFLAG,OUTPUT,JACOB] = fsolve(@fun,x0)

%ohne örtliche Verluste:
%X =
%
 %   1.0976
  %  0.3297
   % 1.0954
    %1.0289
   
    
%    Mit:
 %   X =

  %  1.0901
   % 0.3308
    %1.0784
    %1.0534
