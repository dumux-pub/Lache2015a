

function F=fun(x)
v=0.5;        %Einlassgeschwindigkeit [m/s]
d=0.026;    %kleiner Durchmesser [m]
D=0.066;    %großer Durchmesser [m]
l=0.013;    %Lambda von d
L=0.011;    %Lambda von D
F=[d^2/4*pi*x(1)+D^2/4*pi*x(2)-v*D^2/4*pi;
   d^2/4*pi*x(3)+d^2/4*pi*x(4)-D^2/4*pi*x(2);
   l*135/d*x(3)^2/2/9.81+L*2/D*x(2)^2/2/9.81*2-l*135/d*x(1)^2/2/9.81; %+6*x(3)^2/2/9.81+7*x(2)^2/2/9.81;
   l*139/d*x(4)^2/2/9.81+L*2/D*x(2)^2/2/9.81*2-l*135/d*x(1)^2/2/9.81];   %+L*2/D*x(4)^2/2/9.81*2  +0.8*x(2)^2/2/9.81+0.2*x(4)^2/2/9.81*2];
end


%  x0 = [1; 0.5; 1; 1];   %Make a starting guess at the solution
%  options = optimoptions('fsolve','Display','iter'); %Option to display output
%  [x,fval] = fsolve(@myfun,x0,options)  %Call solver
%oder:
%[X,FVAL,EXITFLAG,OUTPUT,JACOB] = fsolve(@fun,x0)