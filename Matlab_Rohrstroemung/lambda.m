
%Reibungsbeiwert: Übergangsgesetz nach Colebrook-White

%!!!!!!!!!!!!Re UND D anpassen!!!!!!!!!%


function F = lambda(x)
d=0.026;    %kleiner Durchmesser [m]
D=0.066;    %großer Durchmesser [m]
v1=0.666;  %Fließgeschw. erstes Parallelrohr
v=0.1;      %Einlassgeschwindigkeit [m/s]
s=0.000004;      %mittlere äquivalente Sandrauheit PE
k=0.000000365; %kinemat. Viskosität Wasser 80°C
re=v1*d/k                %47441.1 Reynoldszahl dünnes Rohr f. v1=0.666
Re=v*D/k                %18082 v=1
F = [2*log10(2.51/Re/sqrt(x)+s/D/3.71)+1/sqrt(x)];
end


%X(v1=0.666) = 0.0211 kleines Rohr glatt bzw. 0.0217 im Übergangsbereich
%X(v=1) = ?0.0160? 0.0265 großes Rohr glatt bzw. 0.0267 Übergangsbereich