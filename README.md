Summary
=======
This is the DuMuX module containing the code for producing the results
published in:

J. Lache<br>
Entwicklung und Auslegung eines thermischen Frostschutzverfahrens<br>
Bsc Thesis, 2015

You can use the .bib file provided [here](Lache2015a.bib).


Installation
============

The easiest way to install this module is to create a new folder and to execute the file
[installLache2015a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Lache2015a/raw/master/installLache2015a.sh)
in this folder.

```bash
mkdir -p Lache2015a && cd Lache2015a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Lache2015a/raw/master/installLache2015a.sh
sh ./installLache2015a.sh
```

For a detailed information on installation have a look at the DuMuX installation
guide or use the DuMuX handbook, chapter 2.

Applications
============

The applications used for this publication can be found in appl/bsctheses/jonasl.

* __gridcreator__:
  A helper tool to create a customized grid
* __test_kepsilon2cni__:
  The problem setup using a non-isothermal compositional turbulent flow using
  the k-epsilon model
* __test_lowrekepsilon2cni__:
  The problem setup using a non-isothermal compositional turbulent flow using
  a low Reynoldsnumber k-epsilon model
* __test_navierstokes2cni__:
  The problem setup using a non-isothermal compositional flow using the Navier-Stokes equations

In order to run an executable, type e.g.:
```bash
cd dumux-Lache2015a/build-clang/appl/bsctheses/jonasl
make
./test_kepsilon2cni buoyancy.input
```


Used Versions and Software
==========================

For an overview on the used versions of the DUNE and DuMuX modules, please have a look at
[installLache2015a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Lache2015a/raw/master/installLache2015a.sh).

In addition the following external software package are necessary for
compiling the executables:

| software           | version | type          |
| ------------------ | ------- | ------------- |
| cmake              | 3.5.2   | build tool    |
| SuperLU or UMFPack | -       | linear solver |
| ug                 | 3.11.0  | grid manager  |

The module has been checked for the following compilers:

| compiler      | version |
| ------------- | ------- |
| clang/clang++ | 3.5     |
| gcc/g++       | 4.8     |


Additionally to DuMuX, also OpenFoam and Matlab were used, 
see the code used in the folders:
* __Matlab_Rohrstroemung__: hydraulic pipeflow calulations
* __OpenFOAM_Freie_Stroemung__: 2D temperature distribution in the fre-flow domain (air) due to the hot tube
* __OpenFOAM_Rohr__: temperature within the pipe